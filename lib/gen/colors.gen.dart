/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/painting.dart';
import 'package:flutter/material.dart';

class ColorName {
  ColorName._();

  /// Color: #64E986
  static const Color algaeGreen = Color(0xFF64E986);

  /// Color: #F0F8FF
  static const Color aliceBlue = Color(0xFFF0F8FF);

  /// Color: #6CC417
  static const Color alienGreen = Color(0xFF6CC417);

  /// Color: #FAEBD7
  static const Color antiqueWhite = Color(0xFFFAEBD7);

  /// Color: #7FFFD4
  static const Color aquamarine = Color(0xFF7FFFD4);

  /// Color: #827B60
  static const Color armyBrown = Color(0xFF827B60);

  /// Color: #666362
  static const Color ashGray = Color(0xFF666362);

  /// Color: #B2C248
  static const Color avocadoGreen = Color(0xFFB2C248);

  /// Color: #893BFF
  static const Color aztecPurple = Color(0xFF893BFF);

  /// Color: #F0FFFF
  static const Color azure = Color(0xFFF0FFFF);

  /// Color: #95B9C7
  static const Color babyBlue = Color(0xFF95B9C7);

  /// Color: #C25283
  static const Color bashfulPink = Color(0xFFC25283);

  /// Color: #F88158
  static const Color basketBallOrange = Color(0xFFF88158);

  /// Color: #848482
  static const Color battleshipGray = Color(0xFF848482);

  /// Color: #F75D59
  static const Color beanRed = Color(0xFFF75D59);

  /// Color: #E9AB17
  static const Color beeYellow = Color(0xFFE9AB17);

  /// Color: #FBB117
  static const Color beer = Color(0xFFFBB117);

  /// Color: #4C787E
  static const Color beetleGreen = Color(0xFF4C787E);

  /// Color: #F5F5DC
  static const Color beige = Color(0xFFF5F5DC);

  /// Color: #413839
  static const Color blackCat = Color(0xFF413839);

  /// Color: #4C4646
  static const Color blackCow = Color(0xFF4C4646);

  /// Color: #463E3F
  static const Color blackEel = Color(0xFF463E3F);

  /// Color: #FFEBCD
  static const Color blanchedAlmond = Color(0xFFFFEBCD);

  /// Color: #7E3517
  static const Color bloodRed = Color(0xFF7E3517);

  /// Color: #F9B7FF
  static const Color blossomPink = Color(0xFFF9B7FF);

  /// Color: #B7CEEC
  static const Color blueAngel = Color(0xFFB7CEEC);

  /// Color: #4EE2EC
  static const Color blueDiamond = Color(0xFF4EE2EC);

  /// Color: #157DEC
  static const Color blueDress = Color(0xFF157DEC);

  /// Color: #1569C7
  static const Color blueEyes = Color(0xFF1569C7);

  /// Color: #98AFC7
  static const Color blueGray = Color(0xFF98AFC7);

  /// Color: #77BFC7
  static const Color blueHosta = Color(0xFF77BFC7);

  /// Color: #3090C7
  static const Color blueIvy = Color(0xFF3090C7);

  /// Color: #2B547E
  static const Color blueJay = Color(0xFF2B547E);

  /// Color: #659EC7
  static const Color blueKoi = Color(0xFF659EC7);

  /// Color: #8EEBEC
  static const Color blueLagoon = Color(0xFF8EEBEC);

  /// Color: #6960EC
  static const Color blueLotus = Color(0xFF6960EC);

  /// Color: #1F45FC
  static const Color blueOrchid = Color(0xFF1F45FC);

  /// Color: #306EFF
  static const Color blueRibbon = Color(0xFF306EFF);

  /// Color: #342D7E
  static const Color blueWhale = Color(0xFF342D7E);

  /// Color: #57FEFF
  static const Color blueZircon = Color(0xFF57FEFF);

  /// Color: #0041C2
  static const Color blueberryBlue = Color(0xFF0041C2);

  /// Color: #E6A9EC
  static const Color blushPink = Color(0xFFE6A9EC);

  /// Color: #E56E94
  static const Color blushRed = Color(0xFFE56E94);

  /// Color: #B5A642
  static const Color brass = Color(0xFFB5A642);

  /// Color: #FDD017
  static const Color brightGold = Color(0xFFFDD017);

  /// Color: #F433FF
  static const Color brightNeonPink = Color(0xFFF433FF);

  /// Color: #CD7F32
  static const Color bronze = Color(0xFFCD7F32);

  /// Color: #835C3B
  static const Color brownBear = Color(0xFF835C3B);

  /// Color: #E2A76F
  static const Color brownSugar = Color(0xFFE2A76F);

  /// Color: #8C001A
  static const Color burgundy = Color(0xFF8C001A);

  /// Color: #DEB887
  static const Color burlyWood = Color(0xFFDEB887);

  /// Color: #C12267
  static const Color burntPink = Color(0xFFC12267);

  /// Color: #38ACEC
  static const Color butterflyBlue = Color(0xFF38ACEC);

  /// Color: #E38AAE
  static const Color cadillacPink = Color(0xFFE38AAE);

  /// Color: #C19A6B
  static const Color camelBrown = Color(0xFFC19A6B);

  /// Color: #78866B
  static const Color camouflageGreen = Color(0xFF78866B);

  /// Color: #FFA62F
  static const Color cantaloupe = Color(0xFFFFA62F);

  /// Color: #C68E17
  static const Color caramel = Color(0xFFC68E17);

  /// Color: #625D5D
  static const Color carbonGray = Color(0xFF625D5D);

  /// Color: #C12283
  static const Color carnationPink = Color(0xFFC12283);

  /// Color: #50EBEC
  static const Color celeste = Color(0xFF50EBEC);

  /// Color: #34282C
  static const Color charcoal = Color(0xFF34282C);

  /// Color: #8AFB17
  static const Color chartreuse = Color(0xFF8AFB17);

  /// Color: #C24641
  static const Color cherryRed = Color(0xFFC24641);

  /// Color: #C34A2C
  static const Color chestnutRed = Color(0xFFC34A2C);

  /// Color: #C11B17
  static const Color chilliPepper = Color(0xFFC11B17);

  /// Color: #C85A17
  static const Color chocolate = Color(0xFFC85A17);

  /// Color: #C58917
  static const Color cinnamon = Color(0xFFC58917);

  /// Color: #6D6968
  static const Color cloudyGray = Color(0xFF6D6968);

  /// Color: #3EA055
  static const Color cloverGreen = Color(0xFF3EA055);

  /// Color: #0020C2
  static const Color cobaltBlue = Color(0xFF0020C2);

  /// Color: #6F4E37
  static const Color coffee = Color(0xFF6F4E37);

  /// Color: #87AFC7
  static const Color columbiaBlue = Color(0xFF87AFC7);

  /// Color: #F87431
  static const Color constructionConeOrange = Color(0xFFF87431);

  /// Color: #C7A317
  static const Color cookieBrown = Color(0xFFC7A317);

  /// Color: #B87333
  static const Color copper = Color(0xFFB87333);

  /// Color: #FF7F50
  static const Color coral = Color(0xFFFF7F50);

  /// Color: #AFDCEC
  static const Color coralBlue = Color(0xFFAFDCEC);

  /// Color: #FFF380
  static const Color cornYellow = Color(0xFFFFF380);

  /// Color: #151B8D
  static const Color cornflowerBlue = Color(0xFF151B8D);

  /// Color: #FFF8DC
  static const Color cornsilk = Color(0xFFFFF8DC);

  /// Color: #FCDFFF
  static const Color cottonCandy = Color(0xFFFCDFFF);

  /// Color: #9F000F
  static const Color cranberry = Color(0xFF9F000F);

  /// Color: #FFFFCC
  static const Color cream = Color(0xFFFFFFCC);

  /// Color: #E238EC
  static const Color crimson = Color(0xFFE238EC);

  /// Color: #9172EC
  static const Color crocusPurple = Color(0xFF9172EC);

  /// Color: #5CB3FF
  static const Color crystalBlue = Color(0xFF5CB3FF);

  /// Color: #92C7C7
  static const Color cyanOpaque = Color(0xFF92C7C7);

  /// Color: #00FFFF
  static const Color cyanOrAqua = Color(0xFF00FFFF);

  /// Color: #254117
  static const Color darkForrestGreen = Color(0xFF254117);

  /// Color: #AF7817
  static const Color darkGoldenrod = Color(0xFFAF7817);

  /// Color: #F88017
  static const Color darkOrange = Color(0xFFF88017);

  /// Color: #7D1B7E
  static const Color darkOrchid = Color(0xFF7D1B7E);

  /// Color: #E18B6B
  static const Color darkSalmon = Color(0xFFE18B6B);

  /// Color: #8BB381
  static const Color darkSeaGreen = Color(0xFF8BB381);

  /// Color: #2B3856
  static const Color darkSlateBlue = Color(0xFF2B3856);

  /// Color: #25383C
  static const Color darkSlateGrey = Color(0xFF25383C);

  /// Color: #3B9C9C
  static const Color darkTurquoise = Color(0xFF3B9C9C);

  /// Color: #842DCE
  static const Color darkViolet = Color(0xFF842DCE);

  /// Color: #FFCBA4
  static const Color deepPeach = Color(0xFFFFCBA4);

  /// Color: #F52887
  static const Color deepPink = Color(0xFFF52887);

  /// Color: #3BB9FF
  static const Color deepSkyBlue = Color(0xFF3BB9FF);

  /// Color: #79BAEC
  static const Color denimBlue = Color(0xFF79BAEC);

  /// Color: #EDC9AF
  static const Color desertSand = Color(0xFFEDC9AF);

  /// Color: #E3319D
  static const Color dimorphothecaMagenta = Color(0xFFE3319D);

  /// Color: #1589FF
  static const Color dodgerBlue = Color(0xFF1589FF);

  /// Color: #85BB65
  static const Color dollarBillGreen = Color(0xFF85BB65);

  /// Color: #6AFB92
  static const Color dragonGreen = Color(0xFF6AFB92);

  /// Color: #7F525D
  static const Color dullPurple = Color(0xFF7F525D);

  /// Color: #0000A0
  static const Color earthBlue = Color(0xFF0000A0);

  /// Color: #614051
  static const Color eggplant = Color(0xFF614051);

  /// Color: #9AFEFF
  static const Color electricBlue = Color(0xFF9AFEFF);

  /// Color: #5FFB17
  static const Color emeraldGreen = Color(0xFF5FFB17);

  /// Color: #C8B560
  static const Color fallLeafBrown = Color(0xFFC8B560);

  /// Color: #667C26
  static const Color fernGreen = Color(0xFF667C26);

  /// Color: #F70D1A
  static const Color ferrariRed = Color(0xFFF70D1A);

  /// Color: #F62817
  static const Color fireEngineRed = Color(0xFFF62817);

  /// Color: #800517
  static const Color firebrick = Color(0xFF800517);

  /// Color: #F9A7B0
  static const Color flamingoPink = Color(0xFFF9A7B0);

  /// Color: #4E9258
  static const Color forestGreen = Color(0xFF4E9258);

  /// Color: #99C68E
  static const Color frogGreen = Color(0xFF99C68E);

  /// Color: #C9BE62
  static const Color gingerBrown = Color(0xFFC9BE62);

  /// Color: #D4A017
  static const Color gold = Color(0xFFD4A017);

  /// Color: #EAC117
  static const Color goldenBrown = Color(0xFFEAC117);

  /// Color: #EDDA74
  static const Color goldenrod = Color(0xFFEDDA74);

  /// Color: #837E7C
  static const Color granite = Color(0xFF837E7C);

  /// Color: #5E5A80
  static const Color grape = Color(0xFF5E5A80);

  /// Color: #DC381F
  static const Color grapefruit = Color(0xFFDC381F);

  /// Color: #736F6E
  static const Color gray = Color(0xFF736F6E);

  /// Color: #B6B6B4
  static const Color grayCloud = Color(0xFFB6B6B4);

  /// Color: #5C5858
  static const Color grayDolphin = Color(0xFF5C5858);

  /// Color: #D1D0CE
  static const Color grayGoose = Color(0xFFD1D0CE);

  /// Color: #504A4B
  static const Color grayWolf = Color(0xFF504A4B);

  /// Color: #5E7D7E
  static const Color grayishTurquoise = Color(0xFF5E7D7E);

  /// Color: #00FF00
  static const Color green = Color(0xFF00FF00);

  /// Color: #4CC417
  static const Color greenApple = Color(0xFF4CC417);

  /// Color: #6AA121
  static const Color greenOnion = Color(0xFF6AA121);

  /// Color: #89C35C
  static const Color greenPeas = Color(0xFF89C35C);

  /// Color: #6CBB3C
  static const Color greenSnake = Color(0xFF6CBB3C);

  /// Color: #B5EAAA
  static const Color greenThumb = Color(0xFFB5EAAA);

  /// Color: #B1FB17
  static const Color greenYellow = Color(0xFFB1FB17);

  /// Color: #307D7E
  static const Color greenishBlue = Color(0xFF307D7E);

  /// Color: #2C3539
  static const Color gunmetal = Color(0xFF2C3539);

  /// Color: #E66C2C
  static const Color halloweenOrange = Color(0xFFE66C2C);

  /// Color: #EDE275
  static const Color harvestGold = Color(0xFFEDE275);

  /// Color: #617C58
  static const Color hazelGreen = Color(0xFF617C58);

  /// Color: #D462FF
  static const Color heliotropePurple = Color(0xFFD462FF);

  /// Color: #F660AB
  static const Color hotPink = Color(0xFFF660AB);

  /// Color: #7FE817
  static const Color hummingbirdGreen = Color(0xFF7FE817);

  /// Color: #56A5EC
  static const Color iceberg = Color(0xFF56A5EC);

  /// Color: #9CB071
  static const Color iguanaGreen = Color(0xFF9CB071);

  /// Color: #4B0082
  static const Color indigo = Color(0xFF4B0082);

  /// Color: #5EFB6E
  static const Color jadeGreen = Color(0xFF5EFB6E);

  /// Color: #A23BEC
  static const Color jasminePurple = Color(0xFFA23BEC);

  /// Color: #A0CFEC
  static const Color jeansBlue = Color(0xFFA0CFEC);

  /// Color: #46C7C7
  static const Color jellyfish = Color(0xFF46C7C7);

  /// Color: #616D7E
  static const Color jetGray = Color(0xFF616D7E);

  /// Color: #347C2C
  static const Color jungleGreen = Color(0xFF347C2C);

  /// Color: #4CC552
  static const Color kellyGreen = Color(0xFF4CC552);

  /// Color: #ADA96E
  static const Color khaki = Color(0xFFADA96E);

  /// Color: #C5908E
  static const Color khakiRose = Color(0xFFC5908E);

  /// Color: #15317E
  static const Color lapisBlue = Color(0xFF15317E);

  /// Color: #E42217
  static const Color lavaRed = Color(0xFFE42217);

  /// Color: #E3E4FA
  static const Color lavender = Color(0xFFE3E4FA);

  /// Color: #EBDDE2
  static const Color lavenderPinocchio = Color(0xFFEBDDE2);

  /// Color: #87F717
  static const Color lawnGreen = Color(0xFF87F717);

  /// Color: #FFF8C6
  static const Color lemonChiffon = Color(0xFFFFF8C6);

  /// Color: #93FFE8
  static const Color lightAquamarine = Color(0xFF93FFE8);

  /// Color: #ADDFFF
  static const Color lightBlue = Color(0xFFADDFFF);

  /// Color: #E77471
  static const Color lightCoral = Color(0xFFE77471);

  /// Color: #E0FFFF
  static const Color lightCyan = Color(0xFFE0FFFF);

  /// Color: #C3FDB8
  static const Color lightJade = Color(0xFFC3FDB8);

  /// Color: #FAAFBA
  static const Color lightPink = Color(0xFFFAAFBA);

  /// Color: #F9966B
  static const Color lightSalmon = Color(0xFFF9966B);

  /// Color: #3EA99F
  static const Color lightSeaGreen = Color(0xFF3EA99F);

  /// Color: #82CAFA
  static const Color lightSkyBlue = Color(0xFF82CAFA);

  /// Color: #CCFFFF
  static const Color lightSlate = Color(0xFFCCFFFF);

  /// Color: #736AFF
  static const Color lightSlateBlue = Color(0xFF736AFF);

  /// Color: #6D7B8D
  static const Color lightSlateGray = Color(0xFF6D7B8D);

  /// Color: #728FCE
  static const Color lightSteelBlue = Color(0xFF728FCE);

  /// Color: #C8A2C8
  static const Color lilac = Color(0xFFC8A2C8);

  /// Color: #41A317
  static const Color limeGreen = Color(0xFF41A317);

  /// Color: #C48793
  static const Color lipstickPink = Color(0xFFC48793);

  /// Color: #E41B17
  static const Color loveRed = Color(0xFFE41B17);

  /// Color: #7F38EC
  static const Color lovelyPurple = Color(0xFF7F38EC);

  /// Color: #F2BB66
  static const Color macaroniAndCheese = Color(0xFFF2BB66);

  /// Color: #FF00FF
  static const Color magenta = Color(0xFFFF00FF);

  /// Color: #C04000
  static const Color mahogany = Color(0xFFC04000);

  /// Color: #FF8040
  static const Color mangoOrange = Color(0xFFFF8040);

  /// Color: #566D7E
  static const Color marbleBlue = Color(0xFF566D7E);

  /// Color: #810541
  static const Color maroon = Color(0xFF810541);

  /// Color: #43BFC7
  static const Color mascawBlueGreen = Color(0xFF43BFC7);

  /// Color: #E0B0FF
  static const Color mauve = Color(0xFFE0B0FF);

  /// Color: #348781
  static const Color mediumAquamarine = Color(0xFF348781);

  /// Color: #347235
  static const Color mediumForestGreen = Color(0xFF347235);

  /// Color: #B048B5
  static const Color mediumOrchid = Color(0xFFB048B5);

  /// Color: #8467D7
  static const Color mediumPurple = Color(0xFF8467D7);

  /// Color: #306754
  static const Color mediumSeaGreen = Color(0xFF306754);

  /// Color: #348017
  static const Color mediumSpringGreen = Color(0xFF348017);

  /// Color: #48CCCD
  static const Color mediumTurquoise = Color(0xFF48CCCD);

  /// Color: #CA226B
  static const Color mediumVioletRed = Color(0xFFCA226B);

  /// Color: #BCC6CC
  static const Color metallicSilver = Color(0xFFBCC6CC);

  /// Color: #2B1B17
  static const Color midnight = Color(0xFF2B1B17);

  /// Color: #151B54
  static const Color midnightBlue = Color(0xFF151B54);

  /// Color: #FEFCFF
  static const Color milkWhite = Color(0xFFFEFCFF);

  /// Color: #98FF98
  static const Color mintGreen = Color(0xFF98FF98);

  /// Color: #646D7E
  static const Color mistBlue = Color(0xFF646D7E);

  /// Color: #FBBBB9
  static const Color mistyRose = Color(0xFFFBBBB9);

  /// Color: #827839
  static const Color moccasin = Color(0xFF827839);

  /// Color: #493D26
  static const Color mocha = Color(0xFF493D26);

  /// Color: #FFDB58
  static const Color mustard = Color(0xFFFFDB58);

  /// Color: #000080
  static const Color navyBlue = Color(0xFF000080);

  /// Color: #59E817
  static const Color nebulaGreen = Color(0xFF59E817);

  /// Color: #F535AA
  static const Color neonPink = Color(0xFFF535AA);

  /// Color: #78C7C7
  static const Color northernLightsBlue = Color(0xFF78C7C7);

  /// Color: #806517
  static const Color oakBrown = Color(0xFF806517);

  /// Color: #2B65EC
  static const Color oceanBlue = Color(0xFF2B65EC);

  /// Color: #3B3131
  static const Color oil = Color(0xFF3B3131);

  /// Color: #C47451
  static const Color orangeSalmon = Color(0xFFC47451);

  /// Color: #CFECEC
  static const Color paleBlueLily = Color(0xFFCFECEC);

  /// Color: #D16587
  static const Color paleVioletRed = Color(0xFFD16587);

  /// Color: #E56717
  static const Color papayaOrange = Color(0xFFE56717);

  /// Color: #FFFFC2
  static const Color parchment = Color(0xFFFFFFC2);

  /// Color: #B4CFEC
  static const Color pastelBlue = Color(0xFFB4CFEC);

  /// Color: #FFE5B4
  static const Color peach = Color(0xFFFFE5B4);

  /// Color: #FDEEF4
  static const Color pearl = Color(0xFFFDEEF4);

  /// Color: #E9CFEC
  static const Color periwinkle = Color(0xFFE9CFEC);

  /// Color: #FDD7E4
  static const Color pigPink = Color(0xFFFDD7E4);

  /// Color: #387C44
  static const Color pineGreen = Color(0xFF387C44);

  /// Color: #FAAFBE
  static const Color pink = Color(0xFFFAAFBE);

  /// Color: #C48189
  static const Color pinkBow = Color(0xFFC48189);

  /// Color: #FFDFDD
  static const Color pinkBubblegum = Color(0xFFFFDFDD);

  /// Color: #E45E9D
  static const Color pinkCupcake = Color(0xFFE45E9D);

  /// Color: #E799A3
  static const Color pinkDaisy = Color(0xFFE799A3);

  /// Color: #E4287C
  static const Color pinkLemonade = Color(0xFFE4287C);

  /// Color: #E7A1B0
  static const Color pinkRose = Color(0xFFE7A1B0);

  /// Color: #9DC209
  static const Color pistachioGreen = Color(0xFF9DC209);

  /// Color: #E5E4E2
  static const Color platinum = Color(0xFFE5E4E2);

  /// Color: #B93B8F
  static const Color plum = Color(0xFFB93B8F);

  /// Color: #7D0541
  static const Color plumPie = Color(0xFF7D0541);

  /// Color: #583759
  static const Color plumPurple = Color(0xFF583759);

  /// Color: #7D0552
  static const Color plumVelvet = Color(0xFF7D0552);

  /// Color: #C6DEFF
  static const Color powderBlue = Color(0xFFC6DEFF);

  /// Color: #7F5A58
  static const Color puce = Color(0xFF7F5A58);

  /// Color: #F87217
  static const Color pumpkinOrange = Color(0xFFF87217);

  /// Color: #8E35EF
  static const Color purple = Color(0xFF8E35EF);

  /// Color: #6C2DC7
  static const Color purpleAmethyst = Color(0xFF6C2DC7);

  /// Color: #B041FF
  static const Color purpleDaffodil = Color(0xFFB041FF);

  /// Color: #C38EC7
  static const Color purpleDragon = Color(0xFFC38EC7);

  /// Color: #A74AC7
  static const Color purpleFlower = Color(0xFFA74AC7);

  /// Color: #4E387E
  static const Color purpleHaze = Color(0xFF4E387E);

  /// Color: #571B7E
  static const Color purpleIris = Color(0xFF571B7E);

  /// Color: #6A287E
  static const Color purpleJam = Color(0xFF6A287E);

  /// Color: #9E7BFF
  static const Color purpleMimosa = Color(0xFF9E7BFF);

  /// Color: #461B7E
  static const Color purpleMonster = Color(0xFF461B7E);

  /// Color: #7A5DC7
  static const Color purpleSageBush = Color(0xFF7A5DC7);

  /// Color: #FF0000
  static const Color red = Color(0xFFFF0000);

  /// Color: #7F5217
  static const Color redDirt = Color(0xFF7F5217);

  /// Color: #C35817
  static const Color redFox = Color(0xFFC35817);

  /// Color: #990012
  static const Color redWine = Color(0xFF990012);

  /// Color: #BDEDFF
  static const Color robinEggBlue = Color(0xFFBDEDFF);

  /// Color: #C12869
  static const Color roguePink = Color(0xFFC12869);

  /// Color: #E8ADAA
  static const Color rose = Color(0xFFE8ADAA);

  /// Color: #B38481
  static const Color rosyBrown = Color(0xFFB38481);

  /// Color: #7F4E52
  static const Color rosyFinch = Color(0xFF7F4E52);

  /// Color: #2B60DE
  static const Color royalBlue = Color(0xFF2B60DE);

  /// Color: #FFD801
  static const Color rubberDuckyYellow = Color(0xFFFFD801);

  /// Color: #F62217
  static const Color rubyRed = Color(0xFFF62217);

  /// Color: #C36241
  static const Color rust = Color(0xFFC36241);

  /// Color: #FBB917
  static const Color saffron = Color(0xFFFBB917);

  /// Color: #A1C935
  static const Color saladGreen = Color(0xFFA1C935);

  /// Color: #C2B280
  static const Color sand = Color(0xFFC2B280);

  /// Color: #786D5F
  static const Color sandstone = Color(0xFF786D5F);

  /// Color: #EE9A4D
  static const Color sandyBrown = Color(0xFFEE9A4D);

  /// Color: #7E3817
  static const Color sangria = Color(0xFF7E3817);

  /// Color: #2554C7
  static const Color sapphireBlue = Color(0xFF2554C7);

  /// Color: #FF2400
  static const Color scarlet = Color(0xFFFF2400);

  /// Color: #E8A317
  static const Color schoolBusYellow = Color(0xFFE8A317);

  /// Color: #FFF5EE
  static const Color seaShell = Color(0xFFFFF5EE);

  /// Color: #C2DFFF
  static const Color seaBlue = Color(0xFFC2DFFF);

  /// Color: #4E8975
  static const Color seaGreen = Color(0xFF4E8975);

  /// Color: #438D80
  static const Color seaTurtleGreen = Color(0xFF438D80);

  /// Color: #437C17
  static const Color seaweedGreen = Color(0xFF437C17);

  /// Color: #CC6600
  static const Color sedona = Color(0xFFCC6600);

  /// Color: #7F462C
  static const Color sepia = Color(0xFF7F462C);

  /// Color: #347C17
  static const Color shamrockGreen = Color(0xFF347C17);

  /// Color: #E55B3C
  static const Color shockingOrange = Color(0xFFE55B3C);

  /// Color: #8A4117
  static const Color sienna = Color(0xFF8A4117);

  /// Color: #488AC7
  static const Color silkBlue = Color(0xFF488AC7);

  /// Color: #82CAFF
  static const Color skyBlue = Color(0xFF82CAFF);

  /// Color: #737CA1
  static const Color slateBlue = Color(0xFF737CA1);

  /// Color: #657383
  static const Color slateGray = Color(0xFF657383);

  /// Color: #BCE954
  static const Color slimeGreen = Color(0xFFBCE954);

  /// Color: #726E6D
  static const Color smokeyGray = Color(0xFF726E6D);

  /// Color: #4AA02C
  static const Color springGreen = Color(0xFF4AA02C);

  /// Color: #4863A0
  static const Color steelBlue = Color(0xFF4863A0);

  /// Color: #57E964
  static const Color stoplightGoGreen = Color(0xFF57E964);

  /// Color: #FFE87C
  static const Color sunYellow = Color(0xFFFFE87C);

  /// Color: #E67451
  static const Color sunriseOrange = Color(0xFFE67451);

  /// Color: #ECE5B6
  static const Color tanBrown = Color(0xFFECE5B6);

  /// Color: #E78A61
  static const Color tangerine = Color(0xFFE78A61);

  /// Color: #483C32
  static const Color taupe = Color(0xFF483C32);

  /// Color: #CCFB5D
  static const Color teaGreen = Color(0xFFCCFB5D);

  /// Color: #008080
  static const Color teal = Color(0xFF008080);

  /// Color: #D2B9D3
  static const Color thistle = Color(0xFFD2B9D3);

  /// Color: #81D8D0
  static const Color tiffanyBlue = Color(0xFF81D8D0);

  /// Color: #C88141
  static const Color tigerOrange = Color(0xFFC88141);

  /// Color: #7DFDFE
  static const Color tronBlue = Color(0xFF7DFDFE);

  /// Color: #C25A7C
  static const Color tulipPink = Color(0xFFC25A7C);

  /// Color: #43C6DB
  static const Color turquoise = Color(0xFF43C6DB);

  /// Color: #C45AEC
  static const Color tyrianPurple = Color(0xFFC45AEC);

  /// Color: #E55451
  static const Color valentineRed = Color(0xFFE55451);

  /// Color: #565051
  static const Color vampireGray = Color(0xFF565051);

  /// Color: #F3E5AB
  static const Color vanilla = Color(0xFFF3E5AB);

  /// Color: #7E354D
  static const Color velvetMaroon = Color(0xFF7E354D);

  /// Color: #728C00
  static const Color venomGreen = Color(0xFF728C00);

  /// Color: #7E587E
  static const Color violaPurple = Color(0xFF7E587E);

  /// Color: #8D38C9
  static const Color violet = Color(0xFF8D38C9);

  /// Color: #F6358A
  static const Color violetRed = Color(0xFFF6358A);

  /// Color: #EBF4FA
  static const Color water = Color(0xFFEBF4FA);

  /// Color: #FC6C85
  static const Color watermelonPink = Color(0xFFFC6C85);

  /// Color: #C6AEC7
  static const Color wisteriaPurple = Color(0xFFC6AEC7);

  /// Color: #966F33
  static const Color wood = Color(0xFF966F33);

  /// Color: #52D017
  static const Color yellowGreen = Color(0xFF52D017);

  /// Color: #54C571
  static const Color zombieGreen = Color(0xFF54C571);

  /// Color: #F0F8FF
  static const Color aliceblue = Color(0xFFF0F8FF);

  /// Color: #FAEBD7
  static const Color antiquewhite = Color(0xFFFAEBD7);

  /// Color: #00FFFF
  static const Color aqua = Color(0xFF00FFFF);

  /// Color: #FFE4C4
  static const Color bisque = Color(0xFFFFE4C4);

  /// Color: #000000
  static const Color black = Color(0xFF000000);

  /// Color: #FFEBCD
  static const Color blanchedalmond = Color(0xFFFFEBCD);

  /// Color: #0000FF
  static const Color blue = Color(0xFF0000FF);

  /// Color: #8A2BE2
  static const Color blueviolet = Color(0xFF8A2BE2);

  /// Color: #A52A2A
  static const Color brown = Color(0xFFA52A2A);

  /// Color: #DEB887
  static const Color burlywood = Color(0xFFDEB887);

  /// Color: #5F9EA0
  static const Color cadetblue = Color(0xFF5F9EA0);

  /// Color: #6495ED
  static const Color cornflowerblue = Color(0xFF6495ED);

  /// Color: #00FFFF
  static const Color cyan = Color(0xFF00FFFF);

  /// Color: #00008B
  static const Color darkblue = Color(0xFF00008B);

  /// Color: #008B8B
  static const Color darkcyan = Color(0xFF008B8B);

  /// Color: #B8860B
  static const Color darkgoldenrod = Color(0xFFB8860B);

  /// Color: #A9A9A9
  static const Color darkgray = Color(0xFFA9A9A9);

  /// Color: #006400
  static const Color darkgreen = Color(0xFF006400);

  /// Color: #BDB76B
  static const Color darkkhaki = Color(0xFFBDB76B);

  /// Color: #8B008B
  static const Color darkmagenta = Color(0xFF8B008B);

  /// Color: #556B2F
  static const Color darkolivegreen = Color(0xFF556B2F);

  /// Color: #FF8C00
  static const Color darkorange = Color(0xFFFF8C00);

  /// Color: #9932CC
  static const Color darkorchid = Color(0xFF9932CC);

  /// Color: #8B0000
  static const Color darkred = Color(0xFF8B0000);

  /// Color: #E9967A
  static const Color darksalmon = Color(0xFFE9967A);

  /// Color: #8FBC8F
  static const Color darkseagreen = Color(0xFF8FBC8F);

  /// Color: #483D8B
  static const Color darkslateblue = Color(0xFF483D8B);

  /// Color: #2F4F4F
  static const Color darkslategray = Color(0xFF2F4F4F);

  /// Color: #00CED1
  static const Color darkturquoise = Color(0xFF00CED1);

  /// Color: #9400D3
  static const Color darkviolet = Color(0xFF9400D3);

  /// Color: #FF1493
  static const Color deeppink = Color(0xFFFF1493);

  /// Color: #00BFFF
  static const Color deepskyblue = Color(0xFF00BFFF);

  /// Color: #696969
  static const Color dimgray = Color(0xFF696969);

  /// Color: #1E90FF
  static const Color dodgerblue = Color(0xFF1E90FF);

  /// Color: #FFFAF0
  static const Color floralwhite = Color(0xFFFFFAF0);

  /// Color: #228B22
  static const Color forestgreen = Color(0xFF228B22);

  /// Color: #FF00FF
  static const Color fuchsia = Color(0xFFFF00FF);

  /// Color: #DCDCDC
  static const Color gainsboro = Color(0xFFDCDCDC);

  /// Color: #F8F8FF
  static const Color ghostwhite = Color(0xFFF8F8FF);

  /// Color: #F0FFF0
  static const Color honeydew = Color(0xFFF0FFF0);

  /// Color: #FF69B4
  static const Color hotpink = Color(0xFFFF69B4);

  /// Color: #CD5C5C
  static const Color indianred = Color(0xFFCD5C5C);

  /// Color: #FFFFF0
  static const Color ivory = Color(0xFFFFFFF0);

  /// Color: #FFF0F5
  static const Color lavenderblush = Color(0xFFFFF0F5);

  /// Color: #7CFC00
  static const Color lawngreen = Color(0xFF7CFC00);

  /// Color: #FFFACD
  static const Color lemonchiffon = Color(0xFFFFFACD);

  /// Color: #ADD8E6
  static const Color lightblue = Color(0xFFADD8E6);

  /// Color: #F08080
  static const Color lightcoral = Color(0xFFF08080);

  /// Color: #E0FFFF
  static const Color lightcyan = Color(0xFFE0FFFF);

  /// Color: #FAFAD2
  static const Color lightgoldenrodyellow = Color(0xFFFAFAD2);

  /// Color: #90EE90
  static const Color lightgreen = Color(0xFF90EE90);

  /// Color: #D3D3D3
  static const Color lightgrey = Color(0xFFD3D3D3);

  /// Color: #FFB6C1
  static const Color lightpink = Color(0xFFFFB6C1);

  /// Color: #FFA07A
  static const Color lightsalmon = Color(0xFFFFA07A);

  /// Color: #20B2AA
  static const Color lightseagreen = Color(0xFF20B2AA);

  /// Color: #87CEFA
  static const Color lightskyblue = Color(0xFF87CEFA);

  /// Color: #778899
  static const Color lightslategray = Color(0xFF778899);

  /// Color: #B0C4DE
  static const Color lightsteelblue = Color(0xFFB0C4DE);

  /// Color: #FFFFE0
  static const Color lightyellow = Color(0xFFFFFFE0);

  /// Color: #00FF00
  static const Color lime = Color(0xFF00FF00);

  /// Color: #32CD32
  static const Color limegreen = Color(0xFF32CD32);

  /// Color: #FAF0E6
  static const Color linen = Color(0xFFFAF0E6);

  /// Color: #66CDAA
  static const Color mediumaquamarine = Color(0xFF66CDAA);

  /// Color: #0000CD
  static const Color mediumblue = Color(0xFF0000CD);

  /// Color: #BA55D3
  static const Color mediumorchid = Color(0xFFBA55D3);

  /// Color: #9370DB
  static const Color mediumpurple = Color(0xFF9370DB);

  /// Color: #3CB371
  static const Color mediumseagreen = Color(0xFF3CB371);

  /// Color: #7B68EE
  static const Color mediumslateblue = Color(0xFF7B68EE);

  /// Color: #00FA9A
  static const Color mediumspringgreen = Color(0xFF00FA9A);

  /// Color: #48D1CC
  static const Color mediumturquoise = Color(0xFF48D1CC);

  /// Color: #C71585
  static const Color mediumvioletred = Color(0xFFC71585);

  /// Color: #191970
  static const Color midnightblue = Color(0xFF191970);

  /// Color: #F5FFFA
  static const Color mintcream = Color(0xFFF5FFFA);

  /// Color: #FFE4E1
  static const Color mistyrose = Color(0xFFFFE4E1);

  /// Color: #FFDEAD
  static const Color navajowhite = Color(0xFFFFDEAD);

  /// Color: #000080
  static const Color navy = Color(0xFF000080);

  /// Color: #FDF5E6
  static const Color oldlace = Color(0xFFFDF5E6);

  /// Color: #808000
  static const Color olive = Color(0xFF808000);

  /// Color: #6B8E23
  static const Color olivedrab = Color(0xFF6B8E23);

  /// Color: #FFA500
  static const Color orange = Color(0xFFFFA500);

  /// Color: #FF4500
  static const Color orangered = Color(0xFFFF4500);

  /// Color: #DA70D6
  static const Color orchid = Color(0xFFDA70D6);

  /// Color: #EEE8AA
  static const Color palegoldenrod = Color(0xFFEEE8AA);

  /// Color: #98FB98
  static const Color palegreen = Color(0xFF98FB98);

  /// Color: #AFEEEE
  static const Color paleturquoise = Color(0xFFAFEEEE);

  /// Color: #DB7093
  static const Color palevioletred = Color(0xFFDB7093);

  /// Color: #FFEFD5
  static const Color papayawhip = Color(0xFFFFEFD5);

  /// Color: #FFDAB9
  static const Color peachpuff = Color(0xFFFFDAB9);

  /// Color: #CD853F
  static const Color peru = Color(0xFFCD853F);

  /// Color: #B0E0E6
  static const Color powderblue = Color(0xFFB0E0E6);

  /// Color: #BC8F8F
  static const Color rosybrown = Color(0xFFBC8F8F);

  /// Color: #4169E1
  static const Color royalblue = Color(0xFF4169E1);

  /// Color: #8B4513
  static const Color saddlebrown = Color(0xFF8B4513);

  /// Color: #FA8072
  static const Color salmon = Color(0xFFFA8072);

  /// Color: #F4A460
  static const Color sandybrown = Color(0xFFF4A460);

  /// Color: #2E8B57
  static const Color seagreen = Color(0xFF2E8B57);

  /// Color: #FFF5EE
  static const Color seashell = Color(0xFFFFF5EE);

  /// Color: #C0C0C0
  static const Color silver = Color(0xFFC0C0C0);

  /// Color: #87CEEB
  static const Color skyblue = Color(0xFF87CEEB);

  /// Color: #6A5ACD
  static const Color slateblue = Color(0xFF6A5ACD);

  /// Color: #708090
  static const Color slategray = Color(0xFF708090);

  /// Color: #FFFAFA
  static const Color snow = Color(0xFFFFFAFA);

  /// Color: #00FF7F
  static const Color springgreen = Color(0xFF00FF7F);

  /// Color: #4682B4
  static const Color steelblue = Color(0xFF4682B4);

  /// Color: #D2B48C
  static const Color tan = Color(0xFFD2B48C);

  /// Color: #FF6347
  static const Color tomato = Color(0xFFFF6347);

  /// Color: #F5DEB3
  static const Color wheat = Color(0xFFF5DEB3);

  /// Color: #FFFFFF
  static const Color white = Color(0xFFFFFFFF);

  /// Color: #F5F5F5
  static const Color whitesmoke = Color(0xFFF5F5F5);

  /// Color: #FFFF00
  static const Color yellow = Color(0xFFFFFF00);

  /// Color: #9ACD32
  static const Color yellowgreen = Color(0xFF9ACD32);
}
