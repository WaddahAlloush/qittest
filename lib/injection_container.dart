import 'package:dio/dio.dart';

import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:qit_test/features/cart/presentation/cubit/cart_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Core/Api/api_consumer.dart';
import 'Core/Api/dio_consumer.dart';
import 'Core/Api/dio_interceptor.dart';
import 'Core/network/network_info.dart';
import 'features/auth/data/datasources/auth_remote_datasource.dart';
import 'features/auth/data/repositories/auth_repository_imp.dart';
import 'features/auth/domain/repositories/auth_repository.dart';
import 'features/auth/domain/usecases/login_user_usecase.dart';
import 'features/auth/domain/usecases/register_user_usecase.dart';
import 'features/auth/presentation/cubit/auth_cubit.dart';
import 'features/cart/data/datasources/cart_remote_datasource.dart';
import 'features/cart/data/repositories/cart_repository_imp.dart';
import 'features/cart/domain/repositories/cart_repository.dart';
import 'features/cart/domain/usecases/delete_product_usecase.dart';
import 'features/cart/domain/usecases/store_product_usecase.dart';
import 'features/cart/domain/usecases/view_cart_usecase.dart';
import 'features/cart/presentation/cubit/cart_manager_cubit.dart';
import 'features/product/data/datasources/product_remote_datasource.dart';
import 'features/product/data/repositories/product_repository_imp.dart';
import 'features/product/domain/repositories/product_repository.dart';
import 'features/product/domain/usecases/get_product_list_usecase.dart';
import 'features/product/presentation/cubit/product_cubit.dart';



final sl = GetIt.instance;

Future<void> init() async {
  // Bloc

  sl.registerFactory(() => AuthCubit(loginUserUsecase: sl(),
      registerUserUsecase: sl()));
  sl.registerFactory(() => ProductCubit(getProductListUsecase: sl(),
     ));
  sl.registerFactory(() => CartCubit(viewCartUsecase: sl()
     ));
  sl.registerFactory(() => CartManagerCubit(storeProductUsecase: sl(),deleteProductUsecase: sl()
     ));
 
  // Use cases
  sl.registerLazySingleton(() => RegisterUserUsecase(authRepository: sl()));
  sl.registerLazySingleton(() => LoginUserUsecase(authRepository: sl()));
  sl.registerLazySingleton(() => GetProductListUsecase(productRepository: sl()));
  sl.registerLazySingleton(() => StoreProductUsecase(cartRepository: sl()));
  sl.registerLazySingleton(() => DeleteProductUsecase(cartRepository: sl()));
  sl.registerLazySingleton(() => ViewCartUsecase(cartRepository: sl()));
 
  // Repository
  sl.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImp(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );
  sl.registerLazySingleton<CartRepository>(
    () => CartRepositoryImp(
      networkInfo: sl(),
      cartRemoteDatasource: sl(),
    ),
  );
  sl.registerLazySingleton<ProductRepository>(
    () => ProductRepositoryImp(
      networkInfo: sl(),
      productRemoteDataSource: sl(),
    ),
  );
 
  // Data sources

  sl.registerLazySingleton<AuthRemoteDataSource>(
    () => AuthRemoteDataSourceImp(apiConsumer: sl()),
  );
  sl.registerLazySingleton<CartRemoteDatasource>(
    () => CartRemoteDatasourceImp(apiConsumer: sl()),
  );
  sl.registerLazySingleton<ProductRemoteDataSource>(
    () => ProductRemoteDataSourceImp(apiConsumer: sl()),
  );
 

  //! Core

  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton<ApiConsumer>(() => DioConsumer(client: sl()));

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => AppInterceptors());
  sl.registerLazySingleton(() => Dio());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
