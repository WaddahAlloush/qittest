import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qit_test/Core/Helpers/cash_helper.dart';
import 'package:qit_test/Core/translations/locale_keys.g.dart';
import 'package:qit_test/Core/utils/Global%20Widgets/Dialogs/exit_app_dialog.dart';
import 'package:qit_test/Core/utils/Global%20Widgets/blure_button.dart';
import 'package:qit_test/router.dart';

import '../cart/presentation/pages/cart_screen.dart';
import '../product/presentation/pages/products_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // late PageController pageController;

  int currentIndex = 0;
  @override
  void initState() {
    super.initState();
    // pageController = PageController();
  }

  @override
  void dispose() {
    // pageController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> pages = [
      const ProductsScreen(),
      const CartScreen(),
      Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BlurButton(
                onPress: () {
                  CashHelper.clearPrefs().then((value) =>
                      Navigator.pushNamedAndRemoveUntil(
                          context, FRouter.intro, (route) => false));
                },
                child: Text(
                  LocaleKeys.signOut.tr(),
                  style: Theme.of(context).primaryTextTheme.bodyMedium,
                )),
          ],
        ),
      ),
    ];
    return WillPopScope(
      onWillPop: () {
        return showExitAppDialog(context);
      },
      child: Scaffold(
        extendBody: true,
        backgroundColor: Colors.transparent,
        bottomNavigationBar: ConvexAppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 5,
          shadowColor: Colors.white,
          height: 65.h,
          style: TabStyle.react,
          initialActiveIndex: currentIndex,
          top: -20,
          items: const [
            TabItem(
              icon: Icons.home,
            ),
            TabItem(icon: Icons.shopping_cart),
            TabItem(icon: Icons.settings),
          ],
          onTap: (int i) {
            setState(() {
              currentIndex = i;
            });
          },
        ),
        body: pages[currentIndex],
      ),
    );
  }
}
