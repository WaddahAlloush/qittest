
import '../../../../Core/Api/api_consumer.dart';
import '../../../../Core/Api/end_points.dart';
import '../models/user_model.dart';

abstract class AuthRemoteDataSource {
  Future<UserModel> registerUser({required Map authData});

  Future<UserModel> loginUser({required Map authData});
}
class AuthRemoteDataSourceImp implements AuthRemoteDataSource{
   final ApiConsumer apiConsumer;
  AuthRemoteDataSourceImp({
    required this.apiConsumer,
  });
  @override
  Future<UserModel> loginUser({required Map authData}) async{
    var response = await apiConsumer.post(EndPoints.login,queryParameter: {
      'email':authData['email'],
      'password':authData['password'],
    });
    return UserModel.fromJson(response);
  }

  @override
  Future<UserModel> registerUser({required Map authData})async {
     var response = await apiConsumer.post(EndPoints.register,queryParameter: {
       'email':authData['email'],
       'name':authData['name'],
       'password':authData['password'],
       'password_confirmation':authData['password_confirmation'],
     });
    return UserModel.fromJson(response);
  }
}