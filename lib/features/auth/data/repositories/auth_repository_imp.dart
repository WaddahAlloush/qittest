// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';



import '../../../../Core/Helpers/map_exception_to_failure.dart';
import '../../../../Core/error/failures.dart';
import '../../../../Core/network/network_info.dart';
import '../../domain/entities/user.dart';
import '../../domain/repositories/auth_repository.dart';
import '../datasources/auth_remote_datasource.dart';

class AuthRepositoryImp implements AuthRepository {
   final AuthRemoteDataSource remoteDataSource;

  final NetworkInfo networkInfo;
  AuthRepositoryImp({
    required this.remoteDataSource,
    required this.networkInfo,
  });
  @override
  Future<Either<Failure, Userr>> loginUser(Map authData)async {
    if (await networkInfo.isConnected) {
      try {
        final user = await remoteDataSource.loginUser(authData: authData);

        return Right(user);
      } on Exception catch (e) {
        return left(mapExceptionToFailure(e));
      }
    } else {
      return Left(NoInternetFailure());
    }
  }

  @override
  Future<Either<Failure, Userr>> registerUser(Map authData) async{
    if (await networkInfo.isConnected) {
      try {
        final user = await remoteDataSource.registerUser(authData: authData);

        return Right(user);
      } on Exception catch (e) {
        return left(mapExceptionToFailure(e));
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}
