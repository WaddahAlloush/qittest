import '../../domain/entities/user.dart';

class UserModel extends Userr {
  const UserModel(
      {required super.token,
      required super.userName,
      required super.userEmail,
      required super.userId});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
        token: json['token'],
        userName: json['user']['name'],
        userEmail: json['user']['email'],
        userId: json['user']['id'],
       );
  }
}
