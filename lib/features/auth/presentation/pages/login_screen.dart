import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../../../Core/Config/app_strings.dart';
import '../../../../Core/Helpers/cash_helper.dart';
import '../../../../Core/translations/locale_keys.g.dart';
import '../../../../Core/utils/Global Widgets/blure_button.dart';
import '../../../../Core/utils/constants.dart';
import '../../../../router.dart';
import '../cubit/auth_cubit.dart';
import '../widgets/auth_ui.dart';
import '../widgets/border_text_form.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController emailController;
  late TextEditingController passwordController;
  bool isLogedin = false;

  GlobalKey<FormState> fkey = GlobalKey<FormState>();
  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: AuthUI(
        child: BlocConsumer<AuthCubit, AuthState>(
          listener: (context, state) {
            if (state is AuthErrorState) {
              Constants.showAwesomeErrorSnack(
                  context: context, msg: state.error);
            } else if (state is RegistredUserState) {
              isLogedin = true;
              Constants.showAwesomeSuccessSnack(
                  context: context, msg: LocaleKeys.authLoginSuccessMsg.tr());
              Future.delayed(
                const Duration(seconds: 1),
              )
                  .then((value) => CashHelper.saveData(
                      key: AppStrings.userToken, value: state.user.token))
                  .then((value) => CashHelper.saveData(
                      key: AppStrings.userName, value: state.user.userName))
                  .then((value) => CashHelper.saveData(
                      key: AppStrings.userEmail, value: state.user.userEmail))
                  .then((value) => CashHelper.saveData(
                      key: AppStrings.userId, value: state.user.userId))
                  .then(
                    (value) => Navigator.pushNamed(
                      context,
                      FRouter.home,
                    ),
                  );
            }
          },
          builder: (context, state) {
            return SingleChildScrollView(
              child: Form(
                key: fkey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(height: 20.h),
                    Text(LocaleKeys.login.tr(),
                        style:
                            Theme.of(context).primaryTextTheme.headlineMedium),
                    SizedBox(height: 20.h),
                    BorderTextForm(
                        action: TextInputAction.next,
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                        hint: 'user@example.test',
                        validator: MultiValidator([
                          EmailValidator(errorText: LocaleKeys.validEmail.tr()),
                          RequiredValidator(
                              errorText: LocaleKeys.requiredEmail.tr())
                        ])),
                    SizedBox(height: 20.h),
                    BorderTextForm(
                        obsecure: true,
                        action: TextInputAction.next,
                        keyboardType: TextInputType.visiblePassword,
                        controller: passwordController,
                        hint: '***********',
                        validator: MultiValidator([
                          RequiredValidator(
                              errorText: LocaleKeys.requiredEmail.tr())
                        ])),
                    SizedBox(height: 20.h),
                    BlurButton(
                        onPress: state is AuthLoadingState
                            ? () {}
                            : () {
                                if (fkey.currentState!.validate()) {
                                  context.read<AuthCubit>().loginUser({
                                    'email': emailController.text,
                                    'password': passwordController.text,
                                  });
                                }
                                // Navigator.pushNamed(context, FRouter.otp,
                                //     arguments:
                                //         "+${zipController.text}${phoneController.text}");
                              },
                        child: state is AuthLoadingState
                            ? Constants.onLoading()
                            : Text(
                                LocaleKeys.login.tr(),
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .bodyMedium,
                                textAlign: TextAlign.center,
                              )),
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed(FRouter.reg);
                        },
                        child: Text(
                          LocaleKeys.reg.tr(),
                          style:
                              Theme.of(context).primaryTextTheme.headlineSmall,
                        )),
                    TextButton(
                        onPressed: () {
                          Navigator.of(context)
                              .pushReplacementNamed(FRouter.home);
                        },
                        child: Text(
                          LocaleKeys.guest.tr(),
                          style:
                              Theme.of(context).primaryTextTheme.headlineSmall,
                        ))
                  ],
                ),
              ),
            );
          },
        ),
      )),
    );
  }
}
