// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'border_text_form.dart';

class RegFormField extends StatelessWidget {
  final String label;

  final TextEditingController controller;
  bool? isReadOnly;
  List<FieldValidator<dynamic>> validators;
  VoidCallback? ontap;
  RegFormField({
    Key? key,
    required this.label,
    required this.controller,
    this.isReadOnly,
    required this.validators,
    this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 8.w,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            label,
            style: Theme.of(context).primaryTextTheme.headlineSmall,
          ),
          BorderTextForm(
              ontap: ontap,
              action: TextInputAction.next,
              keyboardType: TextInputType.name,
              readOnly: isReadOnly,
              controller: controller,
              hint: "",
              validator: MultiValidator(validators)),
        ],
      ),
    );
  }
}
