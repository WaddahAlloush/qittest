import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BorderTextForm extends StatelessWidget {
  final String hint;

  final TextEditingController controller;
  final TextInputType keyboardType;
  final String? Function(String?) validator;
  final TextInputAction action;
  bool? obsecure;
  bool? readOnly;
  VoidCallback? ontap;
  BorderTextForm({
    Key? key,
    required this.hint,
    required this.controller,
    required this.keyboardType,
    required this.validator,
    required this.action,
    this.obsecure,
    this.readOnly,
    this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h),
      child: TextFormField(
        readOnly: readOnly ?? false,
        onTap: ontap,
        style: Theme.of(context).primaryTextTheme.bodySmall,
        keyboardType: keyboardType,
        textInputAction: action,
        controller: controller,
        validator: validator,
        obscureText: obsecure ?? false,
        decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            hintText: hint,
            border: const OutlineInputBorder(
                borderSide: BorderSide(
              color: Colors.grey,
            )),
            enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
              color: Colors.grey,
            )),
            focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.blue)),
            errorStyle: TextStyle(fontSize: 10.sp),
            errorBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red)),
            hintStyle: Theme.of(context).primaryTextTheme.labelSmall,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIconColor: Theme.of(context).primaryColor,
            contentPadding:
                EdgeInsets.symmetric(horizontal: 10.w, vertical: 0)),
      ),
    );
  }
}
