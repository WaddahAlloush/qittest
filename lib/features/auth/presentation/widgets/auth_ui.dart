// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../gen/assets.gen.dart';

class AuthUI extends StatelessWidget {
  final Widget child;
  const AuthUI({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Assets.images.svg.logo.svg(height: 200.w, width: 200.w),
          SizedBox(
            height: 10.h,
          ),
          Expanded(
              child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 5.h),
            decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                ),
                border: Border.all(
                  color: Colors.white,
                  width: 2,
                )),
            child: child,
          ))
        ],
      ),
    );
  }
}
