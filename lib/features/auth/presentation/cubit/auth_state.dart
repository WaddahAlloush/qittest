// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthLoadingState extends AuthState {}

class AuthErrorState extends AuthState {
  final String error;
  const AuthErrorState({
    required this.error,
  });

  @override
  List<Object> get props => [error];
}

class AuthPhoneSubmittedState extends AuthState {}

class RegistredUserState extends AuthState {
  final Userr user;
 const RegistredUserState({
    required this.user,
  });
   @override
  List<Object> get props => [user];
}

class AuthOtpVerifiedState extends AuthState {}
