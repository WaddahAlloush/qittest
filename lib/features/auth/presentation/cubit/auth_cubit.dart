import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import 'package:flutter_bloc/flutter_bloc.dart';


import '../../../../Core/Helpers/cash_helper.dart';
import '../../../../Core/Helpers/map_failure_to_msg.dart';
import '../../../../Core/error/failures.dart';
import '../../domain/entities/user.dart';
import '../../domain/usecases/login_user_usecase.dart';
import '../../domain/usecases/register_user_usecase.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final LoginUserUsecase loginUserUsecase;
  final RegisterUserUsecase registerUserUsecase;
  late String verficationId;
  int? resend;
  AuthCubit({required this.loginUserUsecase, required this.registerUserUsecase})
      : super(AuthInitial());
  Future<void> loginUser(Map authData) async {
    emit(AuthLoadingState());
    Either<Failure, Userr> userORfailure = await loginUserUsecase(authData);
    emit(_eitherFailureOrUser(userORfailure));
  }
  Future<void> registerUser(Map authData) async {
    emit(AuthLoadingState());
    Either<Failure, Userr> userORfailure = await registerUserUsecase(authData);
    emit(_eitherFailureOrUser(userORfailure));
  }

 
}

AuthState _eitherFailureOrUser(Either<Failure, Userr> either) {
  return either.fold(
    (failure) => AuthErrorState(error: mapFailureToMessage(failure )),
    (user) => RegistredUserState(
      user:user),
  );
}
