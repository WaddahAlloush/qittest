import 'package:dartz/dartz.dart';

import '../../../../Core/error/failures.dart';
import '../entities/user.dart';

abstract class AuthRepository {
  Future<Either<Failure, Userr>> loginUser(Map authData);
  Future<Either<Failure, Userr>> registerUser(Map authData);
}
