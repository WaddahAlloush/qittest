// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:dartz/dartz.dart';


import '../../../../Core/error/failures.dart';
import '../../../../Core/usecases/usecase.dart';
import '../entities/user.dart';
import '../repositories/auth_repository.dart';

class LoginUserUsecase implements UseCase<Userr, Map> {
  final AuthRepository authRepository;
  LoginUserUsecase({
    required this.authRepository,
  });

  @override
  Future<Either<Failure, Userr>> call(Map params) async {
    return await authRepository.loginUser(params);
  }
}
