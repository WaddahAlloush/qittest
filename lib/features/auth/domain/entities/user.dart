// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class Userr extends Equatable {
  final String token;
  final String userName;
  final String userEmail;
  final int userId;

  const Userr({
    required this.token,
    required this.userName,
    required this.userEmail,
    required this.userId,
  });

  @override
  List<Object> get props => [token, userName, userEmail, userId];
}
