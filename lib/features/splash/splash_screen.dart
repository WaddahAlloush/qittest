import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';


import '../../Core/Config/app_strings.dart';
import '../../Core/Helpers/cash_helper.dart';
import '../../gen/assets.gen.dart';
import '../../router.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(
      const Duration(seconds: 4),
      () => Navigator.pushReplacementNamed(
          context,
          CashHelper.getData(key: AppStrings.userToken) != null
              ? FRouter.home
              : CashHelper.getData(key: AppStrings.sereenSteps) == 1
                  ? FRouter.login
                  : FRouter.intro),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Center(
        child: Stack(alignment: Alignment.center,
          children: [
            Lottie.asset(Assets.lottie.stars.path, animate: true , repeat: true),
            Assets.images.svg.logo.svg(),
          ],
        ),
      ),
    );
  }
}
