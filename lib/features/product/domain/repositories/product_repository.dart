import 'package:dartz/dartz.dart';

import '../../../../Core/error/failures.dart';
import '../entities/product.dart';

abstract class ProductRepository {
  Future<Either<Failure, List<Product>>> getProductList(int perpage);
}
