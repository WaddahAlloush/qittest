// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:qit_test/Core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:qit_test/Core/usecases/usecase.dart';

import '../entities/product.dart';
import '../repositories/product_repository.dart';

class GetProductListUsecase implements UseCase<List<Product>, int> {
  final ProductRepository productRepository;
  GetProductListUsecase({
    required this.productRepository,
  });

  @override
  Future<Either<Failure, List<Product>>> call(int params) async {
    return await productRepository.getProductList(params);
  }
}
