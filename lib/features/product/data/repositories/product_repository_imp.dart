// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import 'package:qit_test/Core/error/failures.dart';
import 'package:qit_test/Core/network/network_info.dart';
import 'package:qit_test/features/product/domain/entities/product.dart';

import '../../../../Core/Helpers/map_exception_to_failure.dart';
import '../../domain/repositories/product_repository.dart';
import '../datasources/product_remote_datasource.dart';

class ProductRepositoryImp implements ProductRepository {
  final ProductRemoteDataSource productRemoteDataSource;
  final NetworkInfo networkInfo;
  ProductRepositoryImp({
    required this.productRemoteDataSource,
    required this.networkInfo,
  });
  @override
  Future<Either<Failure, List<Product>>> getProductList(int perpage) async{
   if (await networkInfo.isConnected) {
      try {
        final productsList = await productRemoteDataSource.getProductsList(perpage: perpage);

        return Right(productsList);
      } on Exception catch (e) {
        return left(mapExceptionToFailure(e));
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}
