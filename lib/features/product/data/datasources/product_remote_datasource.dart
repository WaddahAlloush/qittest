// ignore_for_file: public_member_api_docs, sort_constructors_first
import '../../../../Core/Api/api_consumer.dart';
import '../../../../Core/Api/end_points.dart';
import '../models/product_model.dart';

abstract class ProductRemoteDataSource {
  Future<List<ProductModel>> getProductsList({required int perpage});
}
class ProductRemoteDataSourceImp implements ProductRemoteDataSource {
   final ApiConsumer apiConsumer;
  ProductRemoteDataSourceImp({
    required this.apiConsumer,
  });
  
  @override
  Future<List<ProductModel>> getProductsList({required int perpage}) async{
    var response = await apiConsumer.get(EndPoints.products , queryParameter: {
      'perpage':perpage
    });
    return List<ProductModel>.from(
        (response['data'] as List).map((e) => ProductModel.fromJson(e)));
  }
}
