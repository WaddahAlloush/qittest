// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../Core/utils/Global Widgets/details_box.dart';
import '../../../../gen/assets.gen.dart';
import 'color_tag.dart';

class UserServicesWidget extends StatelessWidget {
  final String status;
  final String serviceName;
  final String serviceImg;
  final String serviceDesc;
  final VoidCallback onPress;
  final int id;

  const UserServicesWidget({
    Key? key,
    required this.status,
    required this.serviceName,
    required this.serviceImg,
    required this.serviceDesc,
    required this.onPress,
    required this.id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DetailBox(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: CachedNetworkImage(
                errorWidget: (context, url, error) =>
                    Image.asset(Assets.images.svg.logo.path),
                imageUrl: serviceImg,
                height: 100.w,
                width: 100.w,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: 10.w,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ColorTag(text: status, color: setStatusColor(status)),
                      IconButton(
                          onPressed: onPress,
                          icon: Icon(
                            Icons.shopping_cart_outlined,
                            color: Colors.teal,
                            size: 30.w,
                          )),
                      Text(
                        '#$id',
                        style: Theme.of(context).primaryTextTheme.headlineSmall,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text(
                    serviceName,
                    style: Theme.of(context)
                        .primaryTextTheme
                        .headlineSmall!
                        .copyWith(fontWeight: FontWeight.w900),
                    softWrap: true,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text(
                    serviceDesc,
                    style: Theme.of(context)
                        .primaryTextTheme
                        .labelSmall!
                        .copyWith(fontWeight: FontWeight.w700),
                    softWrap: true,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    ));
  }
}

Color setStatusColor(String status) {
  switch (status) {
    case "Ongoing":
      return Colors.blue;
    case "Completed":
      return Colors.green;

    default:
      return Colors.indigoAccent;
  }
}
