// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../Core/Helpers/shimmer_widget.dart';
import '../../../../Core/utils/Global Widgets/details_box.dart';

class UserServicesShimmer extends StatelessWidget {
  const UserServicesShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DetailBox(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ShimmerWidget.rectangular(
              width: 100.w,
              height: 100.w,
              radius: 15,
            ),
            SizedBox(
              width: 10.w,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ShimmerWidget.rectangular(
                        width: 100.w,
                        height: 20.h,
                        radius: 5,
                      ),
                      ShimmerWidget.circular(
                        width: 30.w,
                        height: 30.h,
                      ),
                      ShimmerWidget.circular(
                        width: 30.w,
                        height: 30.h,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25.h,
                  ),
                  ShimmerWidget.rectangular(
                    width: 150.w,
                    height: 20.h,
                    radius: 5,
                  ),
                  SizedBox(
                    height: 25.h,
                  ),
                  ShimmerWidget.rectangular(
                    width: 250.w,
                    height: 10.h,
                    radius: 5,
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    ));
  }
}
