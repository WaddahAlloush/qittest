// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qit_test/Core/utils/constants.dart';
import 'package:qit_test/features/cart/presentation/cubit/cart_manager_cubit.dart';

import '../../../../Core/translations/locale_keys.g.dart';

Future<bool> storeProductDialog(BuildContext context, int productId) {
  showDialog(
      context: context,
      builder: (context) => AlertDialog(
            buttonPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            alignment: Alignment.center,
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Theme.of(context).primaryColor),
                borderRadius: const BorderRadius.all(Radius.circular(32.0))),
            title: Center(
              child: Text(
                LocaleKeys.productStore.tr(),
                style: Theme.of(context).primaryTextTheme.displayMedium,
              ),
            ),
            content: ChooseQuantity(productId: productId),
            titlePadding: const EdgeInsets.symmetric(vertical: 20),
          ));
  return Future.value(true);
}

class ChooseQuantity extends StatefulWidget {
  final int productId;

  const ChooseQuantity({
    Key? key,
    required this.productId,
  }) : super(key: key);

  @override
  State<ChooseQuantity> createState() => _ChooseQuantityState();
}

class _ChooseQuantityState extends State<ChooseQuantity> {
  int quantity = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
                onPressed: () {
                  if (quantity <= 0) {
                    return;
                  } else {
                    setState(() {
                      quantity--;
                    });
                  }
                },
                icon: const Icon(
                  Icons.remove_circle_outline,
                  color: Colors.white,
                  size: 30,
                )),
            Text(
              quantity.toString(),
              style: Theme.of(context).primaryTextTheme.headlineMedium,
            ),
            IconButton(
                onPressed: () {
                  setState(() {
                    quantity++;
                  });
                },
                icon: const Icon(
                  Icons.add_circle_outline,
                  color: Colors.white,
                  size: 30,
                )),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            BlocListener<CartManagerCubit, CartManagerState>(
              listener: (context, state) {
                if (state is CartManagerErrorState) {
                  Constants.showAwesomeErrorSnack(
                      context: context, msg: state.errorMsg);
                } else if (state is CartManagerLoadedState) {
                  Constants.showAwesomeSuccessSnack(
                      context: context, msg: state.successMsg);
                }
              },
              child: TextButton(
                style: TextButton.styleFrom(backgroundColor: Colors.redAccent),
                onPressed: () {
                  context.read<CartManagerCubit>().storeProduct({
                    "product_id": widget.productId,
                    "quantity": quantity
                  }).then((value) => Navigator.pop(context));
                },
                child: Text(
                  LocaleKeys.save.tr(),
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(backgroundColor: Colors.purple),
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                LocaleKeys.cancle.tr(),
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
            )
          ],
        )
      ],
    );
  }
}
