import 'package:flutter/material.dart';

class ColorTag extends StatelessWidget {
  final String text;
  final Color color;
  const ColorTag({
    Key? key,
    required this.text,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: color.withOpacity(0.2),
          borderRadius: BorderRadius.circular(5)),
      child: Text(
        text,
        style:
            TextStyle(color: color, fontSize: 15, fontWeight: FontWeight.bold),
      ),
    );
  }
}
