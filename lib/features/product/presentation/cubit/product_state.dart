// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'product_cubit.dart';

abstract class ProductState extends Equatable {
  const ProductState();

  @override
  List<Object> get props => [];
}

class ProductInitial extends ProductState {}

class ProductLoadingState extends ProductState {}

class ProductLoadedState extends ProductState {
  final List<Product> productList;
  const ProductLoadedState({
    required this.productList,
  });
  @override
  List<Object> get props => [productList];
}

class ProductErrorState extends ProductState {
  final String errorMsg;
 const ProductErrorState({
    required this.errorMsg,
  });
    @override
  List<Object> get props => [errorMsg];
}
