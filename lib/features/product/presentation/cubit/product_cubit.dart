// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../Core/Helpers/map_failure_to_msg.dart';
import '../../../../Core/error/failures.dart';
import '../../domain/entities/product.dart';
import '../../domain/usecases/get_product_list_usecase.dart';

part 'product_state.dart';

class ProductCubit extends Cubit<ProductState> {
  final GetProductListUsecase getProductListUsecase;
  ProductCubit({
    required this.getProductListUsecase,
  }) : super(ProductInitial());
  Future<void> getProductsPerPage(int perPage) async {
    emit(ProductLoadingState());
     Either<Failure, List<Product>> productListORfailure = await getProductListUsecase(perPage);
    emit(_eitherFailureOrProductList(productListORfailure));
  }
}
ProductState _eitherFailureOrProductList(Either<Failure, List<Product>> either) {
  return either.fold(
    (failure) => ProductErrorState(errorMsg: mapFailureToMessage(failure )),
    (productList) => ProductLoadedState(
      productList:productList),
  );
}