import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:qit_test/features/product/presentation/cubit/product_cubit.dart';

import '../../../../Core/translations/locale_keys.g.dart';
import '../../../../Core/utils/constants.dart';
import '../../../../gen/assets.gen.dart';
import '../widgets/store_product_dialog.dart';
import '../widgets/user_services_shimmer.dart';
import '../widgets/user_services_widget.dart';

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({super.key});

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  @override
  void initState() {
    context.read<ProductCubit>().getProductsPerPage(10);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          LocaleKeys.productList.tr(),
          style: Theme.of(context).primaryTextTheme.headlineMedium,
        ),
        centerTitle: true,
      ),
      body: BlocConsumer<ProductCubit, ProductState>(
        listener: (context, state) {
          if (state is ProductErrorState) {
            Constants.showAwesomeErrorSnack(
                context: context, msg: state.errorMsg);
          }
        },
        builder: (context, state) {
          if (state is ProductLoadedState) {
            if (state.productList.isEmpty) {
              return Constants.emptyListView(
                  img: Assets.lottie.empty.path,
                  text: LocaleKeys.empty.tr(),
                  context: context);
            } else {
              return AnimationLimiter(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: RefreshIndicator(
                    onRefresh: () {
                      return context
                          .read<ProductCubit>()
                          .getProductsPerPage(10);
                    },
                    child: ListView.builder(
                      itemCount: state.productList.length,
                      itemBuilder: (context, index) =>
                          AnimationConfiguration.staggeredGrid(
                        position: index,
                        columnCount: 2,
                        duration: const Duration(milliseconds: 1500),
                        child: FadeInAnimation(
                            delay: const Duration(milliseconds: 500),
                            child: UserServicesWidget(
                              onPress: () => storeProductDialog(
                                  context, state.productList[index].id),
                              status: state.productList[index].price,
                              serviceName: state.productList[index].title,
                              serviceImg: state.productList[index].image,
                              serviceDesc: state.productList[index].description,
                              id: state.productList[index].id,
                            )),
                      ),
                    ),
                  ),
                ),
              );
            }
          } else {
            return RefreshIndicator(
              onRefresh: () {
                return context.read<ProductCubit>().getProductsPerPage(10);
              },
              child: ListView.builder(
                itemCount: 2,
                itemBuilder: (context, index) => const UserServicesShimmer(),
              ),
            );
          }
        },
      ),
    );
  }
}
