import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qit_test/Core/Config/app_strings.dart';
import 'package:qit_test/Core/Helpers/cash_helper.dart';

import '../../Core/translations/locale_keys.g.dart';
import '../../gen/assets.gen.dart';
import '../../router.dart';

class IntroScreen extends StatelessWidget {
  const IntroScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Assets.images.png.lang.image(),
          Text(
            LocaleKeys.applang.tr(),
            style: Theme.of(context).primaryTextTheme.headlineMedium,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                  onTap: () {
                    EasyLocalization.of(context)!
                        .setLocale(const Locale('ar'))
                        .then((value) => CashHelper.saveData(
                            key: AppStrings.sereenSteps, value: 1))
                        .then((value) => Navigator.pushReplacementNamed(
                            context, FRouter.login));
                  },
                  child: Assets.images.png.uae.image(
                    width: 50.w,
                    height: 50.w,
                  )),
              InkWell(
                  onTap: () {
                    EasyLocalization.of(context)!
                        .setLocale(const Locale('en'))
                        .then((value) => CashHelper.saveData(
                            key: AppStrings.sereenSteps, value: 1))
                        .then((value) => Navigator.pushReplacementNamed(
                            context, FRouter.login));
                  },
                  child: Assets.images.png.usa.image(
                    width: 50.w,
                    height: 50.w,
                  )),
            ],
          )
        ],
      ),
    );
  }
}

//  PageViewModel(
//           decoration: const PageDecoration(imageFlex: 2),
//           image: Image.asset(AssetManager.lang),
//           titleWidget: Text(
//             LocaleKeys.applang.tr(),
//             style: Theme.of(context).primaryTextTheme.bodyLarge,
//           ),
