import 'package:dartz/dartz.dart';
import 'package:qit_test/features/cart/domain/entities/cart.dart';

import '../../../../Core/error/failures.dart';

abstract class CartRepository{
  Future<Either<Failure, bool>> storeProduct(Map<String,dynamic> storeData);
  Future<Either<Failure, bool>> deleteProduct(int productId);
  Future<Either<Failure, Cart>> viewCart();
}