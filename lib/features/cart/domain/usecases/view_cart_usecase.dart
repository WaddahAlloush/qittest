// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';
import 'package:qit_test/Core/error/failures.dart';

import 'package:qit_test/Core/usecases/usecase.dart';
import 'package:qit_test/features/cart/domain/repositories/cart_repository.dart';

import '../entities/cart.dart';

class ViewCartUsecase implements UseCase<Cart, NoParams> {
  final CartRepository cartRepository;
  ViewCartUsecase({
    required this.cartRepository,
  });

  @override
  Future<Either<Failure, Cart>> call(NoParams params) async {
    return await cartRepository.viewCart();
  }
}
