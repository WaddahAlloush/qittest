// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';
import 'package:qit_test/Core/error/failures.dart';

import 'package:qit_test/Core/usecases/usecase.dart';
import 'package:qit_test/features/cart/domain/repositories/cart_repository.dart';

import '../entities/cart.dart';

class DeleteProductUsecase implements UseCase<bool, int> {
  final CartRepository cartRepository;
  DeleteProductUsecase({
    required this.cartRepository,
  });

  @override
  Future<Either<Failure, bool>> call(int params) async {
    return await cartRepository.deleteProduct(params);
  }
}
