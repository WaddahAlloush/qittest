// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class CartProducts extends Equatable {
  final int productId;
  final int id;
  final String totalPrice;
  final String unitPrice;
  final int quantity;
 const CartProducts({
    required this.productId,
    required this.id,
    required this.totalPrice,
    required this.unitPrice,
    required this.quantity,
  });

  @override
  List<Object> get props {
    return [
      productId,
      id,
      totalPrice,
      unitPrice,
      quantity,
    ];
  }
}
