// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

import 'package:qit_test/features/cart/domain/entities/cart_products.dart';

class Cart extends Equatable {
  final int cartId;
  final String totalCartPrice;
  final int totalCartItem;
  final List<CartProducts> cartproducts;
  const Cart({
    required this.cartId,
    required this.totalCartPrice,
    required this.totalCartItem,
    required this.cartproducts,
  });

  @override
  List<Object> get props => [cartId, totalCartPrice, totalCartItem , cartproducts];
}
