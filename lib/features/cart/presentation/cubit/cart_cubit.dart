// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qit_test/Core/usecases/usecase.dart';

import '../../../../Core/Helpers/map_failure_to_msg.dart';
import '../../../../Core/error/failures.dart';
import '../../domain/entities/cart.dart';
import '../../domain/usecases/view_cart_usecase.dart';

part 'cart_state.dart';

class CartCubit extends Cubit<CartState> {
  final ViewCartUsecase viewCartUsecase;
  CartCubit({
    required this.viewCartUsecase,
  }) : super(CartInitial());
  Future<void> viewCart() async {
    emit(CartLoadingState());
    Either<Failure, Cart> cartORfailure = await viewCartUsecase(NoParams());
    emit(_eitherFailureOrCart(cartORfailure));
  }
}

CartState _eitherFailureOrCart(Either<Failure, Cart> either) {
  return either.fold(
    (failure) => CartErrorState(errorMsg: mapFailureToMessage(failure)),
    (cart) => CartLoadedState(cart: cart),
  );
}
