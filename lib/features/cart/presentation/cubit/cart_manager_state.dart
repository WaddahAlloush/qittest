// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'cart_manager_cubit.dart';

abstract class CartManagerState extends Equatable {
  const CartManagerState();

  @override
  List<Object> get props => [];
}

class CartManagerInitial extends CartManagerState {}

class CartManagerLoadingState extends CartManagerState {}

class CartManagerLoadedState extends CartManagerState {
  final String successMsg;
 const CartManagerLoadedState({
    required this.successMsg,
  });
   @override
  List<Object> get props => [successMsg];
}

class CartManagerErrorState extends CartManagerState {
    final String errorMsg;
 const CartManagerErrorState({
    required this.errorMsg,
  });
   @override
  List<Object> get props => [errorMsg];
}
