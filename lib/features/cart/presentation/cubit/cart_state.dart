// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'cart_cubit.dart';

abstract class CartState extends Equatable {
  const CartState();

  @override
  List<Object> get props => [];
}

class CartInitial extends CartState {}

class CartLoadingState extends CartState {}

class CartLoadedState extends CartState {
  final Cart cart;
 
  const CartLoadedState({
    required this.cart,
    
  });
  @override
  List<Object> get props => [cart ];
}

class CartErrorState extends CartState {
  final String errorMsg;
const  CartErrorState({
    required this.errorMsg,
  });
    @override
  List<Object> get props => [errorMsg];
}
