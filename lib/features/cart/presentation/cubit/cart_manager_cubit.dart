// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../Core/Helpers/map_failure_to_msg.dart';
import '../../../../Core/error/failures.dart';
import '../../../../Core/translations/locale_keys.g.dart';
import '../../domain/usecases/delete_product_usecase.dart';
import '../../domain/usecases/store_product_usecase.dart';

part 'cart_manager_state.dart';

class CartManagerCubit extends Cubit<CartManagerState> {
  final StoreProductUsecase storeProductUsecase;
  final DeleteProductUsecase deleteProductUsecase;
  CartManagerCubit({
    required this.storeProductUsecase,
    required this.deleteProductUsecase,
  }) : super(CartManagerInitial());
   Future<void> storeProduct(Map<String, dynamic> storeData) async {
    emit(CartManagerLoadingState());
    Either<Failure, bool> storeORfailure = await storeProductUsecase(storeData);
     emit(_eitherFailureOrSuccess(storeORfailure ,LocaleKeys.storeSuccess.tr() ));
  }
  Future<void> deleteProduct(int productId) async {
    emit(CartManagerLoadingState());
    Either<Failure, bool> deleteORfailure = await deleteProductUsecase(productId);
     emit(_eitherFailureOrSuccess(deleteORfailure,LocaleKeys.productDelete.tr()));
  }
}
CartManagerState _eitherFailureOrSuccess(Either<Failure, bool> either , String successMsg ) {
  return either.fold(
    (failure) => CartManagerErrorState(errorMsg: mapFailureToMessage(failure)),
    (cart) => CartManagerLoadedState( successMsg:successMsg ),
  );
}