// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../Core/translations/locale_keys.g.dart';
import '../../../../Core/utils/Global Widgets/details_box.dart';

class CartProductItem extends StatelessWidget {
  final VoidCallback onPress;
  final int id;
  final int productId;
  final int quantity;
  final String totalPrice;
  final String unitPrice;

  const CartProductItem({
    Key? key,
    required this.onPress,
    required this.id,
    required this.productId,
    required this.quantity,
    required this.totalPrice,
    required this.unitPrice,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DetailBox(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '#$id',
                style: Theme.of(context).primaryTextTheme.headlineSmall,
              ),
              Text(
                '#$productId',
                style: Theme.of(context).primaryTextTheme.headlineSmall,
              ),
              IconButton(
                  onPressed: onPress,
                  icon: Icon(
                    Icons.delete,
                    color: Colors.red,
                    size: 30.w,
                  )),
            ],
          ),
          SizedBox(
            height: 5.h,
          ),
          Text(
            " $totalPrice : ${LocaleKeys.totalPrice.tr()}",
            style: Theme.of(context)
                .primaryTextTheme
                .headlineSmall!
                .copyWith(fontWeight: FontWeight.w900),
            softWrap: true,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 5.h,
          ),
          Text(
            "$unitPrice : ${LocaleKeys.unitPrice.tr()}",
            style: Theme.of(context)
                .primaryTextTheme
                .labelSmall!
                .copyWith(fontWeight: FontWeight.w700),
            softWrap: true,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 5.h,
          ),
          Text(
            "$quantity : ${LocaleKeys.totalQuant.tr()}",
            style: Theme.of(context)
                .primaryTextTheme
                .labelSmall!
                .copyWith(fontWeight: FontWeight.w700),
            softWrap: true,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
