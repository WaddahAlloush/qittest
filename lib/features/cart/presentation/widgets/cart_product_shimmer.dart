// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../Core/Helpers/shimmer_widget.dart';
import '../../../../Core/utils/Global Widgets/details_box.dart';

class CartProductShimmer extends StatelessWidget {
  const CartProductShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DetailBox(
        child:  Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                     ShimmerWidget.circular(
                        width: 30.w,
                        height: 30.h,
                      ),
                      ShimmerWidget.circular(
                        width: 30.w,
                        height: 30.h,
                      ),
                      ShimmerWidget.circular(
                        width: 30.w,
                        height: 30.h,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25.h,
                  ),
                  ShimmerWidget.rectangular(
                    width: 250.w,
                    height: 20.h,
                    radius: 5,
                  ),
                  SizedBox(
                    height: 25.h,
                  ),
                  ShimmerWidget.rectangular(
                    width: 250.w,
                    height: 20.h,
                    radius: 5,
                  ),
                  SizedBox(
                    height: 25.h,
                  ),
                  ShimmerWidget.rectangular(
                    width: 250.w,
                    height: 20.h,
                    radius: 5,
                  ),
                ],
              ),);
  }
}
