import 'package:easy_localization/easy_localization.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qit_test/Core/translations/locale_keys.g.dart';
import 'package:qit_test/Core/utils/Global%20Widgets/details_box.dart';
import 'package:qit_test/Core/utils/constants.dart';
import 'package:qit_test/features/cart/presentation/cubit/cart_cubit.dart';
import 'package:qit_test/features/cart/presentation/cubit/cart_manager_cubit.dart';
import 'package:qit_test/features/cart/presentation/widgets/cart_product_item.dart';
import 'package:qit_test/features/cart/presentation/widgets/cart_product_shimmer.dart';

import '../../../../gen/assets.gen.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  void initState() {
    context.read<CartCubit>().viewCart();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocConsumer<CartCubit, CartState>(
          listener: (context, state) {
            if (state is CartErrorState) {
              Constants.showAwesomeErrorSnack(
                  context: context, msg: state.errorMsg);
            } 
          },
          builder: (context, state) {
            if (state is CartLoadedState) {
              return Column(
                children: [
                  FlipCard(
                      front: DetailBox(
                          child: Center(
                        child: Assets.images.svg.logo
                            .svg(width: 200.h, height: 200.h),
                      )),
                      back: DetailBox(
                          child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "${state.cart.totalCartItem} : ${LocaleKeys.totalCartItems.tr()}",
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headlineMedium,
                          ),
                          SizedBox(
                            height: 20.h,
                          ),
                          Text(
                            "${state.cart.totalCartPrice} : ${LocaleKeys.totalCartPrice.tr()}",
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headlineMedium,
                          )
                        ],
                      ))),
                  Expanded(
                      child: ListView.builder(
                    itemCount: state.cart.cartproducts.length,
                    itemBuilder: (context, index) =>
                        BlocListener<CartManagerCubit, CartManagerState>(
                      listener: (context, state) {
                        if (state is CartManagerErrorState) {
                          Constants.showAwesomeErrorSnack(
                              context: context, msg: state.errorMsg);
                        } else if (state is CartManagerLoadedState) {
                          Constants.showAwesomeSuccessSnack(
                              context: context, msg: state.successMsg);
                        }
                      },
                      child: CartProductItem(
                          onPress: () {
                            context
                                .read<CartManagerCubit>()
                                .deleteProduct(
                                    state.cart.cartproducts[index].productId)
                                .then((value) =>
                                    context.read<CartCubit>().viewCart());
                          },
                          id: state.cart.cartproducts[index].id,
                          productId: state.cart.cartproducts[index].productId,
                          quantity: state.cart.cartproducts[index].quantity,
                          totalPrice: state.cart.cartproducts[index].totalPrice,
                          unitPrice: state.cart.cartproducts[index].unitPrice),
                    ),
                  ))
                ],
              );
            } else {
              return Column(
                children: [
                  DetailBox(
                      child: Center(
                    child:
                        Assets.images.svg.logo.svg(width: 200.h, height: 200.h),
                  )),
                  Expanded(
                      child: ListView.builder(
                    itemBuilder: (context, index) => const CartProductShimmer(),
                  ))
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
