import '../../domain/entities/cart_products.dart';

class CartProductsModel extends CartProducts {
  const CartProductsModel(
      {required super.productId,
      required super.id,
      required super.totalPrice,
      required super.unitPrice,
      required super.quantity});
  factory CartProductsModel.fromJson(Map<String, dynamic> json) {
    return CartProductsModel(
        productId: json['product_id'],
        id: json['id'],
        totalPrice: json['total']['formatted'],
        unitPrice: json['unit_price']['formatted'],
        quantity: json['total_quantity']);
  }
}
