import '../../domain/entities/cart.dart';
import '../../domain/entities/cart_products.dart';
import 'cart_products_model.dart';

class CartModel extends Cart {
  const CartModel(
      {required super.cartId,
      required super.totalCartPrice,
      required super.totalCartItem,
      required super.cartproducts});
  factory CartModel.fromJson(Map<String, dynamic> json) {
    return CartModel(
        cartId: json['id'],
        totalCartPrice: json['total']['formatted'],
        totalCartItem: json['items'],
        cartproducts: List<CartProducts>.from(
            json['products'].map((e) => CartProductsModel.fromJson(e))));
  }
}
