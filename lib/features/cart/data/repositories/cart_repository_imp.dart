// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import 'package:qit_test/Core/error/failures.dart';
import 'package:qit_test/features/cart/domain/entities/cart.dart';

import '../../../../Core/Helpers/map_exception_to_failure.dart';
import '../../../../Core/network/network_info.dart';
import '../../domain/repositories/cart_repository.dart';
import '../datasources/cart_remote_datasource.dart';

class CartRepositoryImp implements CartRepository {
  final NetworkInfo networkInfo;
  final CartRemoteDatasource cartRemoteDatasource;
  CartRepositoryImp({
    required this.networkInfo,
    required this.cartRemoteDatasource,
  });
  @override
  Future<Either<Failure, bool>> deleteProduct(int productId) async {
    if (await networkInfo.isConnected) {
      try {
        final isDeleted =
            await cartRemoteDatasource.deleteProduct(productId);

        return Right(isDeleted);
      } on Exception catch (e) {
        return left(mapExceptionToFailure(e));
      }
    } else {
      return Left(NoInternetFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> storeProduct(Map<String, dynamic> storeData)async {
    if (await networkInfo.isConnected) {
      try {
        final isStored =
            await cartRemoteDatasource.storeProduct(storeData);

        return Right(isStored);
      } on Exception catch (e) {
        return left(mapExceptionToFailure(e));
      }
    } else {
      return Left(NoInternetFailure());
    }
  }

  @override
  Future<Either<Failure, Cart>> viewCart() async{
    if (await networkInfo.isConnected) {
      try {
        final cart =
            await cartRemoteDatasource.viewCart();

        return Right(cart);
      } on Exception catch (e) {
        return left(mapExceptionToFailure(e));
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}
