// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:qit_test/Core/Api/api_consumer.dart';

import '../../../../Core/Api/end_points.dart';
import '../../domain/entities/cart.dart';
import '../models/cart_model.dart';

abstract class CartRemoteDatasource {
  Future<bool> storeProduct(Map<String, dynamic> storeData);
  Future<bool> deleteProduct(int productId);
  Future<CartModel> viewCart();
}

class CartRemoteDatasourceImp implements CartRemoteDatasource {
  final ApiConsumer apiConsumer;
  CartRemoteDatasourceImp({
    required this.apiConsumer,
  });
  
  @override
  Future<bool> deleteProduct(int productId) async{
      await apiConsumer.delete(EndPoints.cartItem , queryParameter: {
      'product_id':productId
    });
    return true;
  }
  
  @override
  Future<bool> storeProduct(Map<String, dynamic> storeData)async {
  await apiConsumer.put(EndPoints.cartItem , queryParameter:storeData);
    return true;
  }
  
  @override
  Future<CartModel> viewCart()async {
  var response =   await apiConsumer.get(EndPoints.cart );
    return CartModel.fromJson(response['data']);
  }
}
