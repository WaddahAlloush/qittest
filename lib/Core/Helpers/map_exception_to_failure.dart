

import '../error/exceptions.dart';
import '../error/failures.dart';

Failure mapExceptionToFailure(Exception exception) {
  switch (exception.runtimeType) {
    case NoInternetConnectionException:
      return NoInternetFailure();

    case FetchDataException:
      return FetchDataFailure();
    case BadrequestException:
      return BadRequestFailure();
    case UnauthorizedException:
      return UnauthorizedFailure();
    case UnprocessableContentException:
      return UnprocessableContentFailure();
    case NotFoundException:
      return NotFoundFailure();
    case ConflictException:
      return ConflictFailure();
    case InternalServerErrorException:
      return InternalServerFailure();
    case CachException:
      return CacheFailure();
    case InvalidParametersException:
      return InvalidParametersFailure();
    case OTPinvalidException:
      return OTPinvalidFailure();
    default:
      return UnexpectedFailure();
  }
}
