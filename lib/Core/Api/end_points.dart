class EndPoints {
  static const String baseUrl = 'https://test.qit.company/api';
  static const String register = '$baseUrl/user/register';
  static const String login = '$baseUrl/user/login';
  static const String products = '$baseUrl/product';
  static const String deleteproduct = '$baseUrl/cart/item';
  static const String cartItem = '$baseUrl/cart/item';
  static const String cart = '$baseUrl/cart';
 
}
