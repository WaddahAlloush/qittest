// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:qit_test/Core/Api/status_code.dart';

import '../error/exceptions.dart';
import 'api_consumer.dart';
import 'dio_interceptor.dart';
import 'end_points.dart';

class DioConsumer implements ApiConsumer {
  final Dio client;
  DioConsumer({
    required this.client,
  }) {
    (client.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    client.options
      ..baseUrl = EndPoints.baseUrl
      ..responseType = ResponseType.plain
      ..followRedirects = false
      ..validateStatus = (status) {
        return status! < StatusCode.internalServerError;
      };
    client.interceptors.add(AppInterceptors());
  }
  @override
  Future? get(String path, {Map<String, dynamic>? queryParameter}) async {
    try {
      Response response =
          await client.get(path, queryParameters: queryParameter);
      if (response.statusCode == 200 || response.statusCode == 201) {
        return jsonDecode(response.data.toString());
      } else if (response.statusCode == 401 || response.statusCode == 404) {
        throw UnauthorizedException();
      }
    } on DioError catch (error) {
      _hundleDioError(error);
    }
  }

  @override
  Future? post(String path,
      {Map<String, dynamic>? body,
      bool isFormDataEnabled = false,
      Map<String, dynamic>? queryParameter}) async {
    try {
      Response response = await client.post(path,
          data: isFormDataEnabled ? FormData.fromMap(body!) : body,
          queryParameters: queryParameter);
      if (response.statusCode == 200 || response.statusCode == 201) {
        return jsonDecode(response.data.toString());
      } else if (response.statusCode == 422) {
        throw UnprocessableContentException();
      }else if (response.statusCode == 401) {
        throw UnauthorizedException();
      }
    } on DioError catch (e) {
      _hundleDioError(e);
    }
  }

  dynamic _hundleDioError(DioError error) {
    switch (error.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw FetchDataException();
      case DioErrorType.response:
        switch (error.response!.statusCode) {
          case StatusCode.badRequest:
            throw BadrequestException();
          case StatusCode.unAuthorized:
          case StatusCode.forbidden:
            throw UnauthorizedException();
          case StatusCode.notFound:
            throw NotFoundException();
          case StatusCode.confilect:
            throw ConflictException();
          case StatusCode.internalServerError:
            throw InternalServerErrorException();
        }
        break;

      case DioErrorType.cancel:
        break;
      case DioErrorType.other:
        throw NoInternetConnectionException();
    }
  }

  @override
  Future? delete(String path, {Map<String, dynamic>? queryParameter})async {
      try {
      Response response =
          await client.delete(path, queryParameters: queryParameter);
      if (response.statusCode == 200 || response.statusCode == 201) {
        return jsonDecode(response.data.toString());
      } else if (response.statusCode == 422) {
        throw UnprocessableContentException();
      } else if (response.statusCode == 401) {
        throw UnauthorizedException();
      }
    } on DioError catch (e) {
      _hundleDioError(e);
    }
  }

  @override
  Future? put(String path, {Map<String, dynamic>? queryParameter}) async {
    try {
      Response response =
          await client.put(path, queryParameters: queryParameter);
      if (response.statusCode == 200 || response.statusCode == 201) {
        return jsonDecode(response.data.toString());
      } else if (response.statusCode == 422) {
        throw UnprocessableContentException();
      } else if (response.statusCode == 401) {
        throw UnauthorizedException();
      }
    } on DioError catch (e) {
      _hundleDioError(e);
    }
  }
}
