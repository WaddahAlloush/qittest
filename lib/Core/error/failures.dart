import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  @override
  List<Object> get props => [];
}

// General failures
class UnexpectedFailure extends Failure {}

class CacheFailure extends Failure {}

class NoInternetFailure extends Failure {}

class UnauthorizedFailure extends Failure {}
class UnprocessableContentFailure extends Failure {}

class FetchDataFailure extends Failure {}
class InvalidParametersFailure extends Failure {}
class OTPinvalidFailure extends Failure {}

class BadRequestFailure extends Failure {}

class NotFoundFailure extends Failure {}

class ConflictFailure extends Failure {}

class InternalServerFailure extends Failure {}
