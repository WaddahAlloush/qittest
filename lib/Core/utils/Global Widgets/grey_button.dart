// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qit_test/Core/Config/media_query_ex.dart';

import '../../translations/locale_keys.g.dart';

class GreyButton extends StatelessWidget {
  final VoidCallback onPress;
  final Color color;
  final String text;
  const GreyButton({
    Key? key,
    required this.onPress,
    required this.color,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
     padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: InkWell(
        onTap: onPress,
        child: Container(
            padding: const EdgeInsets.all(8),
            width: context.width / 2,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  blurRadius: 12.r,
                  color: color,
                )
              ],
              borderRadius: BorderRadius.circular(25.r),
              color: color,
            ),
            child: Text(text,
                style: Theme.of(context).primaryTextTheme.bodySmall)),
      ),
    );
  }
}
