// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qit_test/Core/Config/media_query_ex.dart';

class BlurButton extends StatelessWidget {
  final VoidCallback onPress;
  final Widget child;
  const BlurButton({
    Key? key,
    required this.onPress,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: InkWell(
        onTap: onPress,
        child: Container(
            padding: const EdgeInsets.all(8),
            width: context.width / 2,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  blurRadius: 12.r,
                  color: Colors.white,
                )
              ],
              borderRadius: BorderRadius.circular(25.r),
              color: Colors.white70,
            ),
            child: child),
      ),
    );
  }
}
