// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:form_field_validator/form_field_validator.dart';

import '../../translations/locale_keys.g.dart';

class GlobalBubbleTextForm extends StatelessWidget {
  final TextEditingController controller;
  const GlobalBubbleTextForm({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: RequiredValidator(errorText: LocaleKeys.required.tr()),
      controller: controller,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          contentPadding:
              EdgeInsets.symmetric(vertical: 20.h, horizontal: 10.w),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(15.r))),
    );
  }
}
