import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../translations/locale_keys.g.dart';

Future<bool> showExitAppDialog(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) => AlertDialog(
            buttonPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            actionsAlignment: MainAxisAlignment.center,
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Theme.of(context).primaryColor),
                borderRadius: const BorderRadius.all(Radius.circular(32.0))),
            title: Center(
              child: Text(
                LocaleKeys.exitApp.tr(),
                style: Theme.of(context).primaryTextTheme.displayMedium,
              ),
            ),
            titlePadding: const EdgeInsets.symmetric(vertical: 20),
            actions: [
              TextButton(
                style: TextButton.styleFrom(backgroundColor: Colors.redAccent),
                onPressed: () {
                  exit(1);
                },
                child: Text(
                  LocaleKeys.continuee.tr(),
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
              ),
              TextButton(
                style: TextButton.styleFrom(backgroundColor: Colors.purple),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  LocaleKeys.cancle.tr(),
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
              )
            ],
          ));
  return Future.value(true);
}
