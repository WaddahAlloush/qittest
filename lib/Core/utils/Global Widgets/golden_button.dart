// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class GoldenButton extends StatelessWidget {
  final VoidCallback onPress;
  final Widget child;
  final double width;
  const GoldenButton({
    Key? key,
    required this.onPress,
    required this.child,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: InkWell(
        onTap: onPress,
        child: Container(
          padding: const EdgeInsets.all(8),
          width: width,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.r),
            color: Colors.amber,
          ),
          child:child
        ),
      ),
    );
  }
}
