
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qit_test/Core/Config/media_query_ex.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final VoidCallback ontap;
  const CustomButton({
    Key? key,
    required this.text,
    required this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ontap,
      child: Container(
        margin: const EdgeInsets.all(15),
        width: context.width / 2,
        height: 120.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            gradient: const LinearGradient(colors: [
              Color.fromARGB(255, 60, 97, 243),
              Color.fromARGB(255, 117, 146, 240)
            ])),
        child: Center(
          child: Text(
            text,
            style: Theme.of(context).primaryTextTheme.headlineLarge,
          ),
        ),
      ),
    );
  }
}
