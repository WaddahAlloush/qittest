import 'package:flutter/material.dart';

class BorderColorTag extends StatelessWidget {
  final String text;
  final Color color;
  const BorderColorTag({
    Key? key,
    required this.text,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          border: Border.all(color: color, width: 2),
          borderRadius: BorderRadius.circular(15)),
      child: Text(
        text,
        style:
            TextStyle(color: color, fontSize: 15, fontWeight: FontWeight.bold),
      ),
    );
  }
}
