// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  final TextInputType inputType;
  final String? Function(String?) validator;
  bool? obsecure;
  Widget? suffix;
  CustomTextField({
    Key? key,
    required this.label,
    required this.controller,
    required this.inputType,
    required this.validator,
    this.obsecure,
    this.suffix,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(
        horizontal: 40,
      ),
      child: TextFormField(keyboardType: inputType,
        controller: controller,
        validator: validator,
        obscureText: obsecure ?? false,
        style: Theme.of(context).primaryTextTheme.displayMedium,
        decoration: InputDecoration(
            labelText: label, suffixIcon: suffix ?? const SizedBox()),
      ),
    );
  }
}
