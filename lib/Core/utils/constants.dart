import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:lottie/lottie.dart';

import '../translations/locale_keys.g.dart';


class Constants {
  static void showAwesomeSuccessSnack({
    required BuildContext context,
    required String msg,
  }) {
    AnimatedSnackBar.rectangle(LocaleKeys.success.tr(), msg,
            duration: const Duration(seconds: 2),
            snackBarStrategy: RemoveSnackBarStrategy(),
            type: AnimatedSnackBarType.success,
            brightness: Brightness.dark)
        .show(context);
  }

  static void showAwesomeErrorSnack({
    required BuildContext context,
    required String msg,
  }) {
    AnimatedSnackBar.rectangle(LocaleKeys.error.tr(), msg,
            duration: const Duration(seconds: 2),
            snackBarStrategy: RemoveSnackBarStrategy(),
            type: AnimatedSnackBarType.error,
            brightness: Brightness.dark)
        .show(context);
  }

  static Widget onLoading() {
    return const Center(
        child: SpinKitFadingCube(
      color: Colors.black,
      size: 25,
    ));
  }

  static Widget emptyListView(
      {required String img,
      required String text,
      required BuildContext context}) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Lottie.asset(
              img,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            text,
            softWrap: true,
            textAlign: TextAlign.center,
            style: Theme.of(context).primaryTextTheme.bodyMedium,
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
