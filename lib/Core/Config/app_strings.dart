class AppStrings {
  static const String userToken = 'token';
 
  static const String userName = 'userName';
  static const String userEmail = 'userEmail';
  static const String userId = 'userId';
 
  static const String sereenSteps = 'step';
}
