import 'package:flutter/material.dart';

class LightAppColor {
  static Color primaryColor = const Color.fromARGB(255, 12, 75, 151);
  static Color primaryColorLight = const Color(0xFFFA9C41);
  static Color primaryColorDark = Colors.grey.shade100;
  static Color backgroundColor = Colors.white;
  static Color shadowColor = Colors.black;
  static Color displayColor = const Color(0xFF004785);
  static Color headlineColor = Colors.white;
  static Color titleColor = const Color(0xFF6E95FB);
  static Color bodyColor = Colors.black;
  static Color labelColor = const Color.fromARGB(255, 113, 112, 114);
}

class DarkAppColor {
  static Color primaryColor = Color.fromARGB(255, 20, 186, 100);
  static Color primaryColorLight =  Colors.white;
  static Color primaryColorDark = const Color.fromARGB(255, 49, 48, 48);
  static Color backgroundColor = Colors.black;
  static Color shadowColor = Colors.grey;
  static Color displayColor = Colors.white;
  static Color headlineColor = Colors.white;
  static Color titleColor = Colors.white;
  static Color bodyColor = Colors.black;
  static Color labelColor = const Color.fromARGB(255, 179, 175, 184);
}
