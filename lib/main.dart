import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qit_test/features/cart/presentation/cubit/cart_cubit.dart';
import 'package:qit_test/router.dart';

import 'Core/Config/Themes/app_theme.dart';
import 'Core/Helpers/cash_helper.dart';
import 'features/auth/presentation/cubit/auth_cubit.dart';
import 'features/cart/presentation/cubit/cart_manager_cubit.dart';
import 'features/product/presentation/cubit/product_cubit.dart';
import 'injection_container.dart' as di;
import 'Core/translations/codegen_loader.g.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  FRouter.defineRoutes();

  await di.init();
  await EasyLocalization.ensureInitialized();
  await CashHelper.init();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
  runApp(MultiBlocProvider(
      providers:  [
        BlocProvider(create: (context) => di.sl<AuthCubit>(),),
        BlocProvider(create: (context) => di.sl<ProductCubit>(),),
        BlocProvider(create: (context) => di.sl<CartCubit>(),),
        BlocProvider(create: (context) => di.sl<CartManagerCubit>(),),
      ],
      child: EasyLocalization(
          path: "assets/lang",
          supportedLocales: const [Locale('en'), Locale('ar')],
          fallbackLocale: const Locale('en'),
          assetLoader: const CodegenLoader(),
          saveLocale: true,
          startLocale: const Locale('ar'),
          useOnlyLangCode: true,
          child: const MyApp())));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(414, 941),
        builder: (context, child) {
          return MaterialApp(
            title: 'QIT Test',
            debugShowCheckedModeBanner: false,
            localizationsDelegates: context.localizationDelegates,
            supportedLocales: context.supportedLocales,
            locale: context.locale,
            theme: AppTheme.darkTheme,
            onGenerateRoute: FRouter.router.generator,
            initialRoute: '/',
          );
        });
  }
}
