import 'package:fluro/fluro.dart';

import 'features/auth/presentation/pages/login_screen.dart';
import 'features/auth/presentation/pages/reg_screen.dart';
import 'features/home/home.dart';
import 'features/intro/intro_screen.dart';
import 'features/splash/splash_screen.dart';

class FRouter {
  static final router = FluroRouter();

  static const splash = "/";
  static const intro = "/intro";
  static const login = "/login";

  static const reg = "/reg";

  static const home = "/home";

  //Handler for every route
  static var splashScreen = Handler(
    handlerFunc: (context, parameters) => const SplashScreen(),
  );
  static var loginScreen = Handler(
    handlerFunc: (context, parameters) => const LoginScreen(),
  );
  static var registerScreen = Handler(
    handlerFunc: (context, parameters) => const RegScreen(),
  );
  static var homeScreen = Handler(
    handlerFunc: (context, parameters) => const HomeScreen(),
  );
  static var introScreen = Handler(
    handlerFunc: (context, parameters) => const IntroScreen(),
  );

//================ Define Routes ===============
  static dynamic defineRoutes() {
    router.define(splash,
        handler: splashScreen,
        transitionType: TransitionType.cupertinoFullScreenDialog);
    router.define(login,
        handler: loginScreen, transitionType: TransitionType.inFromTop);
    router.define(reg,
        handler: registerScreen, transitionType: TransitionType.fadeIn);
    router.define(home,
        handler: homeScreen, transitionType: TransitionType.fadeIn);
    router.define(intro,
        handler: introScreen, transitionType: TransitionType.fadeIn);
  }
}
